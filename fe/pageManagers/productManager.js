import * as constants from "../constants.js";
import errorHandler from "../generalTools/errorHandler.js";
import htmlHelper from "../generalTools/htmlHelper.js";
import requestHelper from "../generalTools/requestHelper.js";
import modalDialogHelper from "../generalTools/modalDialogHelper.js";
import formHelper from "../generalTools/formHelper.js";
import Validator from "../generalTools/validator.js";
import { zwaInt, zwaString } from "../generalTools/baseValidations.js";
import accountManager from "./accountManager.js";
import paginationManager from "./paginationManager.js";
import Observable from "../generalTools/observable.js";
import currencyCodes from "../generalTools/currencyCodes.js";
import locationHelper from "../generalTools/locationHelper.js";

class ProductManager extends Observable {
    constructor() {
        super();

        this.productConstants = {
            sortOptions: {
                name: "NAME",
                new: "NEW"
            },
            sortTexts: {
                name: "Podle abecedy",
                new: "Od nejnovějšího"
            },

            defaultSortOption: "NAME",
            defaultPageIndex: 0,
            defaultPageSize: 12,

            loadingErrorMessage: "Error loading the product list.",

            addingErrorMessage: "Error adding the new product.",
            addProductFormId: "addProductForm",
            addProductNameId: "addProductName",
            addProductDescriptionId: "addProductDescription",
            addProductPriceId: "addProductPrice",
            addProductStockId: "addProductStock",
            addProductSubmitButtonId: "addProductSubmitButton",

            productDetailPriceId: "productDetailPrice",

            orderingErrorMessage: "Error ordering the product.",
            orderProductFormId: "orderProductForm",
            orderProductCountId: "orderProductCount",
            orderProductPriceId: "orderProductPrice",
            orderProductTotalPriceId: "orderProductTotalPrice",
            orderProductSubmitButtonId: "orderProductSubmitButton",
            orderSuccessfulMessage: "Objednávka úspěšně dokončena."
        };

        this.loadParams = {
            sort: this.productConstants.defaultSortOption,
            search: "",
            pageIndex: this.productConstants.defaultPageIndex,
            pageSize: this.productConstants.defaultPageSize
        };
    }

    /**
     * Loads product list from BE using XMLHttpRequest
     */
    async loadProductList(loadParams) {
        try {
            // Update loadParams if necessary
            if (loadParams?.sort !== undefined && loadParams?.sort !== null) {
                this.loadParams.sort = loadParams.sort;
            }
            if (loadParams?.search !== undefined && loadParams?.search !== null) {
                this.loadParams.search = loadParams.search;
            }
            if (loadParams?.pageIndex !== undefined && loadParams?.pageIndex !== null) {
                this.loadParams.pageIndex = loadParams.pageIndex;
            }
            if (loadParams?.pageSize !== undefined && loadParams?.pageSize !== null) {
                this.loadParams.pageSize = loadParams.pageSize;
            }

            // Prepare and send request
            const request = new XMLHttpRequest();
            const params = requestHelper.getQueryParamString({...this.loadParams});
            request.open("GET", `${constants.getProductListURL}?${params}`, true);
            request.onload = () => this.handleProductListLoaded.bind(this)(request, loadParams?.witholdObserverNotification);
            request.onerror = errorHandler.handleRequestError;
            request.ontimeout = errorHandler.getRequestTimeoutHandler("Loading the product list timed out.");
            request.timeout = constants.defaultRequestTimeout;
            request.send();
        } catch (error) {
            errorHandler.handleError(error, this.productConstants.loadingErrorMessage, true);
        }
    }

    /**
     * Parses response json and displays the results.
     * @param {*} request
     */
    handleProductListLoaded(request, witholdObserverNotification) {
        const response = JSON.parse(request.responseText);

        if (!requestHelper.isOk(response)) {
            errorHandler.handleError(response.error, `${this.productConstants.loadingErrorMessage}\nMessage: ${response.error.message}`, true);
            return;
        }

        // Display products
        this.displayProducts(response.productList);
        
        // Display pagination
        paginationManager.setPagination.bind(paginationManager)(
            response.pageInfo,
            (pageIndex) => this.loadProductList({pageIndex})
        );

        
        // Notify observers
        if (!witholdObserverNotification) {
            let observerData = {
                tabButtonId : constants.productsTabButtonId,
                tabId: constants.productsTabId,
                loadParams: Object.assign({}, this.loadParams)
            }
            observerData.loadParams.witholdObserverNotification = true;
            this.notifyObservers(observerData);
        } else { // Load happened as a result of going through history
            // sort button might need fixing
            this.setSortButtonState(this.loadParams.sort);

            // search field might need fixing
            document.getElementById(constants.searchTextFieldId).value = this.loadParams.search;
        }
    }

    /**
     * Clears previous data and displays the new product list.
     * @param {*} productList 
     */
    displayProducts(productList) {
        const productsTab = document.getElementById(constants.productsTabId);
        htmlHelper.clearChildren(productsTab);
        if (productList.length) {
            productList.forEach(product => this.addSingleProduct(product, productsTab));
        } else {
            this.showEmptyProductList(productsTab);
        }
    }

    /**
     * If an empty product list is returned,
     * then this function is called to show the message to the user.
     * @param {*} productsTab 
     */
    showEmptyProductList(productsTab) {
        const nothingFound = document.createElement("div");
        nothingFound.classList.add(constants.nothingFoundClass);
        nothingFound.innerHTML = "Nic nenalezeno";
        productsTab.append(nothingFound);
    }

    /**
     * Creates new productTileContainer and productTile.
     * Adds them to the productsTab.
     */
    addSingleProduct(product, productsTab) {
        const productId = this.getHtmlProductId(product);
        const escapedProduct = htmlHelper.escapeHTMLObjectValues(product);

        const productHtml = `
        <div class="${constants.productTileContainerClass}">
            <div id="${productId}" class="${constants.productTileClass}">
            </div>
        </div>
        `;
        const productElement = htmlHelper.createElementFromString(productHtml);

        const productTile = productElement.querySelector(`#${productId}`);;
        
        this.displayMinimizedProductDetail(product, productTile);

        productsTab.append(productElement);
    }

    displayMinimizedProductDetail(product, productTile) {
        const escapedProduct = htmlHelper.escapeHTMLObjectValues(product);

        const minimizedProductDetail = this.getMinimizedProductDetail(escapedProduct);
        htmlHelper.clearChildren(productTile);
        productTile.classList.remove(constants.productDetailClass);
        productTile.append(minimizedProductDetail);

        // Open deatil function
        // Using setTimeout so it executes in the next digest cycle.
        // Otherwise it would be called during closing the product detail.
        setTimeout(() => productTile.onclick = () => this.handleProductDeatilButtonClick(product.id));
    }

    /**
     * Creates and returns a dom element.
     */
    getMinimizedProductDetail(escapedProduct) {
        const minimizedProductDetailHtml = `<div>${escapedProduct.name}</div>`;
        const minimizedProductDetailElement = htmlHelper.createElementFromString(minimizedProductDetailHtml);
        return minimizedProductDetailElement;
    }

    /**
     * Reloads the current product detail if any.
     * Called after login / logout.
     */
    async reloadCurrentProductDetail() {
        try {
            const productDetailTile = document.querySelector(`.${constants.productTileClass}.${constants.productDetailClass}`);
            
            if (!productDetailTile) {
                return;
            }

            const id = this.getProductId(productDetailTile);

            this.handleProductDeatilButtonClick(id);
        } catch (error) {
            errorHandler.handleError(error, "Error reloading the product detail.", true);
        }
    }

    /**
     * Opens / Closes product detail.
     * @param {*} product 
     */
    async handleProductDeatilButtonClick(id) {
        try {
            // Get product detail info
            // Prepare and send request
            const request = new XMLHttpRequest();
            const params = requestHelper.getQueryParamString({id});
            request.open("GET", `${constants.getProductURL}?${params}`, true);
            request.onload = () => this.handleProductLoaded.bind(this)(request);
            request.onerror = errorHandler.handleRequestError;
            request.ontimeout = errorHandler.getRequestTimeoutHandler("Loading the product detail timed out.");
            request.timeout = constants.defaultRequestTimeout;
            request.send();
        } catch (error) {
            errorHandler.handleError(error, "Error opening the product detail.", true);
        }
    }

    /**
     * Parses response json and displays the results.
     * @param {*} request
     */
     async handleProductLoaded(request) {
        const response = JSON.parse(request.responseText);

        if (!requestHelper.isOk(response)) {
            errorHandler.handleError(response.error, `${this.productConstants.loadingErrorMessage}\nMessage: ${response.error.message}`, true);
            return;
        }

        // Display product detail
        const productDetail = response;
        const productTile = document.getElementById(this.getHtmlProductId(productDetail));
        productTile.onclick = null;
        htmlHelper.clearChildren(productTile);
        productTile.classList.add(constants.productDetailClass);
        const fullProductDetail = this.getFullProductDetail(productDetail, productTile);
        productTile.append(fullProductDetail);
    }

    /**
     * Creates and returns a dom element.
     */
    getFullProductDetail(productDetail, productTile) {
        let showOrderButton = false;
        let orderRowHtml = "";
        const orderButtonId = "orderButton";
        if (parseInt(productDetail.stock) === 0) {
            orderRowHtml = `
                <div class="productDetailRow orderRow">
                    <div id="cantOrderMessage">Nelze objednat. Není skladem.</div>
                </div>
            `;
        } else if (!accountManager.authentication) {
            orderRowHtml = `
                <div class="productDetailRow orderRow">
                    <div id="cantOrderMessage">Pro objednání je potřeba přihlášení.</div>
                </div>
            `;
        } else {
            showOrderButton = true;
            orderRowHtml = `
                <div class="productDetailRow orderRow">
                    <button id="${orderButtonId}" title="Objednat">Objednat</button>
                </div>
            `;
        }

        const escapedProductDetail = htmlHelper.escapeHTMLObjectValues(productDetail);
        const closeProductDetailButtonClass = "closeProductDetailButton";
        const fullProductDetailHtml = `
            <div id="productDetails">
                <div class="productDetailRow">
                    <div class="productDetailName">${escapedProductDetail.name}</div>
                    <button class="${closeProductDetailButtonClass}">
                        <img src="fe/resources/img/closeIcon.svg" alt="closeIcon">
                    </button>
                </div>
                <div class="productDetailRow">
                    <div>Popis:</div>
                    <div>${escapedProductDetail.description}</div>
                </div>
                <div class="productDetailRow">
                    <div>Měna: </div>
                    <label class="radioContainer">
                      <input type="radio" name="currency" value="${currencyCodes.czk}" checked="checked">
                      <span class="radioCheckmark"></span>
                      Kč
                    </label>
                    <label class="radioContainer">
                      <input type="radio" name="currency" value="${currencyCodes.eur}">
                      <span class="radioCheckmark"></span>
                      Euro
                    </label>
                </div>
                <div class="productDetailRow">
                    <div>Cena:</div>
                    <div id="${this.productConstants.productDetailPriceId}">${escapedProductDetail.price} Kč</div>
                </div>
                <div class="productDetailRow">
                    <div>Skladem:</div>
                    <div>${escapedProductDetail.stock} ks</div>
                </div>
                ${orderRowHtml}
            </div>
        `;
        const fullProductDetailElement = htmlHelper.createElementFromString(fullProductDetailHtml);

        // Set up currency listners
        const productDetailPrice = fullProductDetailElement.querySelector(`#${this.productConstants.productDetailPriceId}`);
        const currencyCzk = fullProductDetailElement.querySelector(`input[value="${currencyCodes.czk}"]`);
        currencyCzk.onclick = () => productDetailPrice.innerText = `${escapedProductDetail.price} Kč`;

        const currencyEur = fullProductDetailElement.querySelector(`input[value="${currencyCodes.eur}"]`);
        currencyEur.onclick = () => productDetailPrice.innerText = `${String(escapedProductDetail.priceEur).replace(".", ",")} Euro`;

        locationHelper.isCzech() ? currencyCzk.click() : currencyEur.click();

        // Set up close button
        const closeButton = fullProductDetailElement.querySelector(`.${closeProductDetailButtonClass}`);
        const product = {
            id: productDetail.id,
            name: productDetail.name
        };
        closeButton.onclick = (event) => {
            event.preventDefault();
            this.displayMinimizedProductDetail(product, productTile);
        };

        // Set up order button
        if (showOrderButton) {
            const orderButton = fullProductDetailElement.querySelector(`#${orderButtonId}`);
            orderButton.onclick = (event) => this.handleOrderButtonClick.bind(this)(event, productDetail, currencyEur.checked ? currencyCodes.eur : currencyCodes.czk);
        }

        return fullProductDetailElement;
    }

    /**
     * Orders the product.
     * @param {*} product 
     */
     async handleOrderButtonClick(event, productDetail, currency) {
        try {
            event.preventDefault();

            const escapedProductDetail = htmlHelper.escapeHTMLObjectValues(productDetail);
            const orderProductHeader = escapedProductDetail.name;
            const orderProductHeaderHtml = `
                <form id="${this.productConstants.orderProductFormId}" novalidate>
                    <div class="modalRow">
                        <div>Měna: </div>
                        <label class="radioContainer">
                          <input type="radio" name="currency" value="${currencyCodes.czk}" checked="checked">
                          <span class="radioCheckmark"></span>
                          Kč
                        </label>
                        <label class="radioContainer">
                          <input type="radio" name="currency" value="${currencyCodes.eur}">
                          <span class="radioCheckmark"></span>
                          Euro
                        </label>
                    </div>
                    <div class="modalRow">
                        <div>Cena za kus: </div>
                        <div id="${this.productConstants.orderProductPriceId}">
                            ${currency === currencyCodes.czk ? productDetail.price + " Kč" : String(productDetail.priceEur).replace(".", ",") + " Euro"}
                        </div>
                    </div>
                    <div class="modalRow">
                        ${formHelper.createZwaInput(this.productConstants.orderProductCountId, "Počet*", null)}
                    </div>
                    <div class="modalRow">
                        <div>Cena celkem: </div>
                        <div id="${this.productConstants.orderProductTotalPriceId}">0 ${currency === currencyCodes.czk ? "Kč" : "Euro"}</div>
                    </div>
                    <div class="modalRow modalRowSubmit">
                        <button id="${this.productConstants.orderProductSubmitButtonId}" class="modalRowSubmitButton" title="Objednat">Objednat</button>
                    </div>
                </form>
            `;
            const orderProductForm = htmlHelper.createElementFromString(orderProductHeaderHtml);

            const currencyCzk = orderProductForm.querySelector(`input[value="${currencyCodes.czk}"]`);
            const currencyEur = orderProductForm.querySelector(`input[value="${currencyCodes.eur}"]`);
            const orderProductPrice = orderProductForm.querySelector(`#${this.productConstants.orderProductPriceId}`);
            const orderProductCount = orderProductForm.querySelector(`#${this.productConstants.orderProductCountId}`);
            const orderProductTotalPrice = orderProductForm.querySelector(`#${this.productConstants.orderProductTotalPriceId}`);
            
            let updateTotalPrice = () => {
                const orderProductCountValue = orderProductCount.value ? parseInt(orderProductCount.value) : 0;
                const price = currencyCzk.checked ? productDetail.price : productDetail.priceEur;
                const totalPrice = parseFloat((orderProductCountValue * price).toFixed(2));
                const currencyShortName = currencyCzk.checked ? "Kč" : "Euro";
                orderProductTotalPrice.innerText = `${String(totalPrice).replace(".", ",")} ${currencyShortName}`;
            }
            currencyCzk.onclick = () => {
                orderProductPrice.innerText = `${productDetail.price} Kč`
                updateTotalPrice();
            };
            currencyEur.onclick = () => {
                orderProductPrice.innerText = `${String(productDetail.priceEur).replace(".", ",")} Euro`
                updateTotalPrice();
            };

            currency === currencyCodes.eur ? currencyEur.click() : currencyCzk.click();

            // Validation
            const orderProductCountValidation = new zwaInt(true, 1, productDetail.stock);
            const orderProductValidationTypes = {
                [this.productConstants.orderProductCountId]: orderProductCountValidation
            }
            const orderProductValidator = new Validator(orderProductForm, orderProductValidationTypes);

            // Update total price
            orderProductCount.addEventListener("input", () => {
                if (orderProductValidator.validateInput(this.productConstants.orderProductCountId, orderProductCountValidation)) {
                    updateTotalPrice();
                }
            });

            // Submit button
            const orderProductSubmitButton = orderProductForm.querySelector(`#${this.productConstants.orderProductSubmitButtonId}`);
            orderProductSubmitButton.onclick = (event) => this.handleOrderProductSubmitButtonClick(event, orderProductValidator, productDetail);

            modalDialogHelper.openModalDialog(orderProductHeader, orderProductForm);
        } catch (error) {
            errorHandler.handleError(error, null, true);
        }
    }

    /**
     * Validates the order product form and sends the XMLHttpRequest to the BE.
     * @param {*} event 
     * @param {*} orderProductValidator 
     */
     async handleOrderProductSubmitButtonClick(event, orderProductValidator, productDetail) {
        try {
            event.preventDefault();

            // Check form validity
            if (!orderProductValidator.validateForm()) {
                return;
            }

            // Prepare order product data
            const orderProductId = parseInt(productDetail.id);
            const orderProductCount = parseInt(document.getElementById(this.productConstants.orderProductCountId).value);
            const orderProductCurrency = document.querySelector(`#${this.productConstants.orderProductFormId} input[type="radio"][name="currency"]:checked`).value;

            const orderProductData = JSON.stringify({
                orderProductId,
                orderProductCount,
                orderProductCurrency
            });

            // Send order product data to BE
            const request = new XMLHttpRequest();
            request.open("POST", constants.orderProductURL, true);
            request.setRequestHeader(accountManager.accountConstants.authorizationCookieName, `Bearer ${accountManager.bearerToken}`);
            request.setRequestHeader('Content-Type', 'application/json');
            request.onload = () => this.handleOrderProductLoaded.bind(this)(request, orderProductValidator, productDetail);
            request.onerror = (error) => console.warn(`${this.productConstants.orderingErrorMessage} Message: ${JSON.stringify(error, null, 2)}`);
            request.ontimeout = (progressEvent) => console.warn("Sending order product data to BE timed out.");
            request.timeout = constants.defaultRequestTimeout;
            request.send(orderProductData);
        } catch (error) {
            errorHandler.handleError(error, "Error submitting the order product data.", true);
        }
    }

    async handleOrderProductLoaded(request, orderProductValidator, productDetail) {
        try {
            const response = JSON.parse(request.responseText);

            // Handle response errors
            if (!requestHelper.isOk(response)) {
                if (requestHelper.containsValidationErrors(response)) {
                    // Handle form validation results
                    orderProductValidator.showBeValidationResults(response.error.validationResults);
                } else {
                    // Handle general be errors.
                    errorHandler.handleError(response.error, `${this.productConstants.orderingErrorMessage}\nMessage: ${response.error.message}`, true);
                }
                return;
            }

            // Reload product detail
            this.handleProductDeatilButtonClick(productDetail.id);

            // Show success
            modalDialogHelper.openModalDialog(this.productConstants.orderSuccessfulMessage, null,true);
        } catch (error) {
            errorHandler.handleError(error, this.productConstants.orderingErrorMessage, true);
        }
    }

    /**
     * Return id for / of html element.
     * @param {*} product 
     * @returns string
     */
    getHtmlProductId(product) {
        return `productId_${product.id}`;
    }

    /**
     * Returns product id
     * @param {*} productTile 
     * @returns string
     */
    getProductId(productTile) {
        return productTile.id.split("_")[1];
    }

    /**
     * Loads productList using search filter.
     * @param {*} event 
     */
    async search(event) {
        try {
            const searchText = document.getElementById(constants.searchTextFieldId).value;
            this.loadProductList({search: searchText, pageIndex: this.productConstants.defaultPageIndex});
            event.preventDefault();
        } catch (error) {
            errorHandler.handleError(error, "Could not execute search command.", true);
        }
    }

    /**
     * Loads productList using sort filter.
     * @param {*} event 
     */
     async sort(event, sortOption) {
        if (!sortOption)
            return;

        try {
            event.preventDefault();
            
            this.setSortButtonState(sortOption);
            
            this.loadProductList({sort: sortOption});
        } catch (error) {
            errorHandler.handleError(error, "Could not execute sort command.", true);
        }
    }

    /**
     * Sets sort button innerText and onclick event.
     * @param {*} sortOption 
     * @returns 
     */
    setSortButtonState(sortOption) {
        if (!sortOption)
            return;

        let nextSortOption;
        let currentSortText;
        if (sortOption === this.productConstants.sortOptions.name) {
            nextSortOption = this.productConstants.sortOptions.new;
            currentSortText = this.productConstants.sortTexts.name;
        } else {
            nextSortOption = this.productConstants.sortOptions.name;
            currentSortText = this.productConstants.sortTexts.new;
        }

        
        const sortButton = document.getElementById(constants.sortButtonId);
        sortButton.onclick = (event) => this.sort(event, nextSortOption);
        sortButton.querySelector(`span`).innerText = currentSortText;
    }

    /**
     * Opens add product modal dialog form.
     */
    async addProduct() {
        try {
            const addProductHeader = "Přidat produkt";
            const addProductHeaderHtml = `
                <form id="${this.productConstants.addProductFormId}" novalidate>
                    <div class="modalRow">
                        ${formHelper.createZwaInput(this.productConstants.addProductNameId, "Název*")}
                    </div>
                    <div class="modalRow">
                        ${formHelper.createZwaInput(this.productConstants.addProductDescriptionId, "Popis")}
                    </div>
                    <div class="modalRow">
                        ${formHelper.createZwaInput(this.productConstants.addProductPriceId, "Cena [Kč]*")}
                    </div>
                    <div class="modalRow">
                        ${formHelper.createZwaInput(this.productConstants.addProductStockId, "Počet kusů*")}
                    </div>
                    <div class="modalRow modalRowSubmit">
                        <button id="${this.productConstants.addProductSubmitButtonId}" class="modalRowSubmitButton" title="Přidat produkt">
                            <img src="fe/resources/img/addIcon.svg" alt="addIcon">
                            <span>Přidat produkt</span>
                        </button>
                    </div>
                </form>
            `;
            const addProductForm = htmlHelper.createElementFromString(addProductHeaderHtml);

            const addProductValidationTypes = {
                [this.productConstants.addProductNameId]: new zwaString(true, null, 100),
                [this.productConstants.addProductDescriptionId]: new zwaString(false, null, 255),
                [this.productConstants.addProductPriceId]: new zwaInt(true, 0, null),
                [this.productConstants.addProductStockId]: new zwaInt(true, 0, null),
            }

            const addProductValidator = new Validator(addProductForm, addProductValidationTypes);

            const addProductSubmitButton = addProductForm.querySelector(`#${this.productConstants.addProductSubmitButtonId}`);
            addProductSubmitButton.onclick = (event) => this.handleAddProductSubmitButtonClick(event, addProductValidator);

            modalDialogHelper.openModalDialog(addProductHeader, addProductForm);
        } catch (error) {
            errorHandler.handleError(error, null, true);
        }
    }

    /**
     * Validates the add product form and sends the XMLHttpRequest to the BE.
     * @param {*} event 
     * @param {*} addProductValidator 
     */
     async handleAddProductSubmitButtonClick(event, addProductValidator) {
        try {
            event.preventDefault();

            // Check form validity
            if (!addProductValidator.validateForm()) {
                return;
            }

            // Prepare add product data
            const addProductName = document.getElementById(this.productConstants.addProductNameId).value;
            const addProductDescription = document.getElementById(this.productConstants.addProductDescriptionId).value;
            const addProductPrice = parseInt(document.getElementById(this.productConstants.addProductPriceId).value);
            const addProductStock = parseInt(document.getElementById(this.productConstants.addProductStockId).value);

            const addProductData = JSON.stringify({
                [this.productConstants.addProductNameId]: addProductName,
                [this.productConstants.addProductDescriptionId]: addProductDescription,
                [this.productConstants.addProductPriceId]: addProductPrice,
                [this.productConstants.addProductStockId]: addProductStock
            });

            // Send add product data to BE
            const request = new XMLHttpRequest();
            request.open("POST", constants.addProductURL, true);
            request.setRequestHeader(accountManager.accountConstants.authorizationCookieName, `Bearer ${accountManager.bearerToken}`);
            request.setRequestHeader('Content-Type', 'application/json');
            request.onload = () => this.handleAddProductLoaded.bind(this)(request, addProductValidator);
            request.onerror = (error) => console.warn(`${this.productConstants.addingErrorMessage} Message: ${JSON.stringify(error, null, 2)}`);
            request.ontimeout = (progressEvent) => console.warn("Sending add product data to BE timed out.");
            request.timeout = constants.defaultRequestTimeout;
            request.send(addProductData);
        } catch (error) {
            errorHandler.handleError(error, "Error submitting the add product data.", true);
        }
    }

    async handleAddProductLoaded(request, addProductValidator) {
        try {

            const response = JSON.parse(request.responseText);

            // Handle response errors
            if (!requestHelper.isOk(response)) {
                if (requestHelper.containsValidationErrors(response)) {
                    // Handle form validation results
                    addProductValidator.showBeValidationResults(response.error.validationResults);
                } else {
                    // Handle general be errors.
                    errorHandler.handleError(response.error, `${this.productConstants.addingErrorMessage}\nMessage: ${response.error.message}`, true);
                }
                return;
            }

            // Reload product list
            this.loadProductList();

            //Close add product modal dialog
            modalDialogHelper.closeModalDialog();
        } catch (error) {
            errorHandler.handleError(error, this.productConstants.addingErrorMessage, true);
        }
    }
}

export default new ProductManager();