import * as constants from "../constants.js";
import errorHandler from "../generalTools/errorHandler.js";
import htmlHelper from "../generalTools/htmlHelper.js";
import requestHelper from "../generalTools/requestHelper.js";
import modalDialogHelper from "../generalTools/modalDialogHelper.js";

class aboutManager {
    constructor() {
        this.errorMessage = "Error opening the about modal dialog.";
    }

    /**
     * Sends XMLHttpRequest to BE to load about data.
     */
    async openAboutModalDialog() {
        try {
            const request = new XMLHttpRequest();
            request.open("GET", constants.getAboutURL, true);
            request.onload = () => this.handleAboutLoaded.bind(this)(request);;
            request.onerror = errorHandler.handleRequestError;
            request.ontimeout = errorHandler.getRequestTimeoutHandler("Loading the about info timed out.");
            request.timeout = constants.defaultRequestTimeout;
            request.send();
        } catch (error) {
            errorHandler.handleError(error, this.errorMessage, true);
        }
    }

    /**
     * Shows the aboutModalDialog.
     */
    handleAboutLoaded(request) {
        const response = JSON.parse(request.responseText);

        if (!requestHelper.isOk(response)) {
            errorHandler.handleError(response.error, `${this.errorMessage}\nMessage: ${response.error.message}`, true);
            return;
        }

        const escapedResponse = htmlHelper.escapeHTMLObjectValues(response);

        const aboutHeader = "ZWA Online Shop";

        const aboutHtml = `
            <div>
                <div class="modalRow">
                    <div class="label">Git url:</div>
                    <a href="${escapedResponse.gitUrl}" target="_blank">${escapedResponse.gitUrl}</a>
                </div>
                <div class="modalRow">
                    <div class="label">Git branch:</div>
                    <div>${escapedResponse.gitBranch}</div>
                </div>
                <div class="modalRow">
                    <div class="label">Commit date:</div>
                    <div>${escapedResponse.commitDate}</div>
                </div>
                <div class="modalRow">
                    <div class="label">Commit hash:</div>
                    <div>${escapedResponse.commitHash}</div>
                </div>
            </div>
        `;
        const aboutContent = htmlHelper.createElementFromString(aboutHtml);

        modalDialogHelper.openModalDialog(aboutHeader, aboutContent, true);
    }
}

export default new aboutManager();