import * as constants from "../constants.js";
import errorHandler from "../generalTools/errorHandler.js";
import logger from "../generalTools/logger.js";
import logLevels from "../generalTools/logLevels.js";

class themeManager {
    constructor() {
        this.currentTheme = constants.defaultTheme;
    }

    /**
     * Changes css and set the currentTheme variable.
     * Displays the the html body.
     */
    async loadThemeFromLocalStorage() {
        try {
            // Load theme from local storage
            let theme = await this.getThemeFromLocalStorageOrDefault();
            // Check if theme value is valid. Otherwise set default.
            if (!Object.values(constants.themes).includes(theme)) {
                theme = constants.defaultTheme;
            }
            // Set the correct theme
            if (!(await this.changeTheme(theme))) {
                // If the default theme was kept, then no change occured and setThemesSelectValue() was not called.
                // So we need to call it here.
                this.setThemesSelectValue();
            }
        } catch (error) {
            logger.log("Error loading theme from local storage.", logLevels.warning, error.stack);
        } finally {
            // Even if the loading fails we still need to display the page.
            // Default theme from css will be used.
            this.displayHtmlBody();
        }
    }

    /**
     * Return theme from local storage or if none found, then returns default theme.
     * @returns 
     */
    async getThemeFromLocalStorageOrDefault() {
        return localStorage.getItem(constants.themeKey) ?? constants.defaultTheme;
    }

    /**
     * Changes the theme of the page.
     * @param {*} newTheme 
     * @returns true if a change occured. false if default was kept and no change was necessary.
     */
    async changeTheme(newTheme) {
        try {
            // Check whether a change is necessary
            if (newTheme === this.currentTheme) {
                return false;
            } else {
                this.currentTheme = newTheme;
                await this.saveNewTheme(newTheme);
            }
    
            // Set active button style
            this.setThemesSelectValue();
    
            // Set the new theme
            const rootStyle = document.documentElement.style;
            switch (newTheme) {
                case constants.themes.redThemeId:
                    this.setRedTheme(rootStyle);
                    break;
                case constants.themes.greenThemeId:
                    this.setGreenTheme(rootStyle);
                    break;
                case constants.themes.blueThemeId:
                default:
                    this.setBlueTheme(rootStyle);
                    break;
            }
    
            return true;
        } catch (error) {
            errorHandler.handleError(error, `Error changing theme to ${newTheme}`, true);
        }
    }

    /**
     * Saves the new theme to local storage
     */
    async saveNewTheme(newTheme) {
        localStorage.setItem(constants.themeKey, newTheme);
    }

    /**
     * Sets active theme button style.
     */
    setThemesSelectValue() {
        const themesButtons = document.querySelectorAll(`#${constants.themesContainerId} .${constants.themeButtonClass}`);
        themesButtons.forEach(themeButton => {
            if (themeButton.id === this.currentTheme) {
                themeButton.classList.add(constants.activeThemeClass);
            } else {
                themeButton.classList.remove(constants.activeThemeClass);
            }
        });
    }

    /**
     * Sets all rootStyle parameters according to the blue theme.
     * @param {*} rootStyle 
     */
    setBlueTheme(rootStyle) {
        rootStyle.setProperty("--headerColor", "#ffffff");
        rootStyle.setProperty("--headerAndFooterBackground", "url(resources/img/blackNoiseTexture_303841.png) #303841");
        rootStyle.setProperty("--headerAndFooterButtonColor", "#ffffff");
        rootStyle.setProperty("--headerAndFooterButtonBackground", "#2a67a5");
        rootStyle.setProperty("--headerAndFooterButtonBorderColor", "#1a2025");
        rootStyle.setProperty("--headerImageFilter", "invert(1)");

        rootStyle.setProperty("--bodyBackground", "url(resources/img/darkBlueNoiseTexture_2a67a5.png) #2a67a5");
        rootStyle.setProperty("--searchInputColor", "#000000");
        rootStyle.setProperty("--searchFormBackground", "#f0f0ff");
        rootStyle.setProperty("--tabElementBoxShadow", "#1b436b");
        rootStyle.setProperty("--tabButtonColor", "#000000");
        rootStyle.setProperty("--tabButtonInactiveBackground", "#afafb6");
        rootStyle.setProperty("--tabButtonActiveBackground", "#f0f0ff");
        rootStyle.setProperty("--tabImageFilter", "invert(0)");

        rootStyle.setProperty("--productTileColor", "#000000");
        rootStyle.setProperty("--productTileBackground", "#f0f0ff");

        rootStyle.setProperty("--paginationColor", "#ffffff");
        rootStyle.setProperty("--paginationActiveColor", "#000000");
        rootStyle.setProperty("--paginationActiveBackground", "#f0f0ff");
        rootStyle.setProperty("--paginationHoverBackground", "#afafb6");

        rootStyle.setProperty("--footerColor", "#c4c4c4");
        rootStyle.setProperty("--footerImageFilter", "invert(0.85)");

        this.setThemeButtonBackgrounds(rootStyle);

        rootStyle.setProperty("--modalBodyBackground", "#f0f0ff");
        rootStyle.setProperty("--modalBodyHeaderColor", "#000000");

        rootStyle.setProperty("--errorColor", "#ff5050");
    }

    /**
     * Sets all rootStyle parameters according to the green theme.
     * @param {*} rootStyle 
     */
     setGreenTheme(rootStyle) {
        rootStyle.setProperty("--headerColor", "#f0f0ff");
        rootStyle.setProperty("--headerAndFooterBackground", "url(resources/img/darkGreenNoiseTexture_24362c.png) #24362c");
        rootStyle.setProperty("--headerAndFooterButtonColor", "#f0f0ff");
        rootStyle.setProperty("--headerAndFooterButtonBackground", "#439758");
        rootStyle.setProperty("--headerAndFooterButtonBorderColor", "#000000");
        rootStyle.setProperty("--headerImageFilter", "invert(1)");

        rootStyle.setProperty("--bodyBackground", "url(resources/img/greenNoiseTexture_439758.png) #439758");
        rootStyle.setProperty("--searchInputColor", "#ffffff");
        rootStyle.setProperty("--searchFormBackground", "#24362c");
        rootStyle.setProperty("--tabElementBoxShadow", "#cdecdb");
        rootStyle.setProperty("--tabButtonColor", "#f0f0ff");
        rootStyle.setProperty("--tabButtonInactiveBackground", "#577062");
        rootStyle.setProperty("--tabButtonActiveBackground", "#24362c");
        rootStyle.setProperty("--tabImageFilter", "invert(1)");

        rootStyle.setProperty("--productTileColor", "#f0f0ff");
        rootStyle.setProperty("--productTileBackground", "#24362c");

        rootStyle.setProperty("--paginationColor", "#24362c");
        rootStyle.setProperty("--paginationActiveColor", "#f0f0ff");
        rootStyle.setProperty("--paginationActiveBackground", "#24362c");
        rootStyle.setProperty("--paginationHoverBackground", "#577062");

        rootStyle.setProperty("--footerColor", "#c4c4c4");
        rootStyle.setProperty("--footerImageFilter", "invert(1)");

        this.setThemeButtonBackgrounds(rootStyle);

        rootStyle.setProperty("--modalBodyBackground", "#f0fff0");
        rootStyle.setProperty("--modalBodyHeaderColor", "#000000");

        rootStyle.setProperty("--errorColor", "#ff5050");
    }

    /**
     * Sets all rootStyle parameters according to the red theme.
     * @param {*} rootStyle 
     */
     setRedTheme(rootStyle) {
        rootStyle.setProperty("--headerColor", "#ffffff");
        rootStyle.setProperty("--headerAndFooterBackground", "url(resources/img/darkRedNoiseTexture_2e2626.png) #2e2626");
        rootStyle.setProperty("--headerAndFooterButtonColor", "#ffeaef");
        rootStyle.setProperty("--headerAndFooterButtonBackground", "#923535");
        rootStyle.setProperty("--headerAndFooterButtonBorderColor", "#ffeaef");
        rootStyle.setProperty("--headerImageFilter", "invert(1)");

        rootStyle.setProperty("--bodyBackground", "url(resources/img/redNoiseTexture_923535.png) #923535");
        rootStyle.setProperty("--searchInputColor", "#000000");
        rootStyle.setProperty("--searchFormBackground", "#ffeaef");
        rootStyle.setProperty("--tabElementBoxShadow", "#301d1d");
        rootStyle.setProperty("--tabButtonColor", "#000000");
        rootStyle.setProperty("--tabButtonInactiveBackground", "#ad9a9d");
        rootStyle.setProperty("--tabButtonActiveBackground", "#ffeaef");
        rootStyle.setProperty("--tabImageFilter", "invert(0)");

        rootStyle.setProperty("--productTileColor", "#000000");
        rootStyle.setProperty("--productTileBackground", "#ffeaef");

        rootStyle.setProperty("--paginationColor", "#ffeaef");
        rootStyle.setProperty("--paginationActiveColor", "#000000");
        rootStyle.setProperty("--paginationActiveBackground", "#ffeaef");
        rootStyle.setProperty("--paginationHoverBackground", "#ad9a9d");

        rootStyle.setProperty("--footerColor", "#c4c4c4");
        rootStyle.setProperty("--footerImageFilter", "invert(0.85)");

        this.setThemeButtonBackgrounds(rootStyle);

        rootStyle.setProperty("--modalBodyBackground", "#fff0f0");
        rootStyle.setProperty("--modalBodyHeaderColor", "#000000");

        rootStyle.setProperty("--errorColor", "#ff5050");
    }

    /**
     * Theme butons have always the same background: blue, green, red
     */
    setThemeButtonBackgrounds(rootStyle) {
        rootStyle.setProperty("--blueThemeBackground", "#30669c");
        rootStyle.setProperty("--greenThemeBackground", "#439758");
        rootStyle.setProperty("--redThemeBackground", "#923535");
    }

    /**
     * By default the html body is set to display none so we can first set the correct theme.
     * Otherwise there would be some color flashes.
     */
    displayHtmlBody() {
        document.body.style.display = "flex";
    }
}

export default new themeManager();