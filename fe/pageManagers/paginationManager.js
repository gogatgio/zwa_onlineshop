import errorHandler from "../generalTools/errorHandler.js";
import htmlHelper from "../generalTools/htmlHelper.js";

class PaginationManager {
    constructor() {
        this.paginationConstants = {
            /**
             * Number of pages shown from the current pageIndex
             */
            paginationRange: 2,

            paginationId: "pagination",
            activeClass: "active",

            paginationErrorMessage: "Error displaying pagination."
        };

        this.pagination = document.getElementById(this.paginationConstants.paginationId);
    }

    /**
     * Hides pagination by setting emptyPageInfo.
     */
    async setEmptyPagination() {
        const emptyPageInfo = {
            pageIndex: 0,
            pageSize: 0,
            pageCount: 0
        };

        this.setPagination(emptyPageInfo, () => {});
    }

    /**
     * Creates and displays the pagination buttons.
     * Sets their onClick to the given loadPage function.
     * @param {*} pageInfo 
     * @param {*} loadPage function of type (pageIndex) => {}
     */
    async setPagination(pageInfo, loadPage) {
        try {
            // Clear previsous pagination
            htmlHelper.clearChildren(this.pagination);

            // Show left button
            const showLeftButton = pageInfo.pageIndex > 0;
            if (showLeftButton) {
                this.pagination.append(this.getSinglePaginationButton("&laquo;", () => loadPage(0)));
            }

            // Show numbered page buttons
            const minPageIndex = Math.max(pageInfo.pageIndex - this.paginationConstants.paginationRange, 0);
            const maxPageIndex = Math.min(pageInfo.pageIndex + this.paginationConstants.paginationRange, pageInfo.pageCount - 1);
            for (let i = minPageIndex; i <= maxPageIndex; i++) {
                const active = i === pageInfo.pageIndex;
                this.pagination.append(this.getSinglePaginationButton(i + 1, () => loadPage(i), active));
            }

            // Show right button
            const showRightButton = pageInfo.pageIndex < pageInfo.pageCount - 1;
            if (showRightButton) {
                this.pagination.append(this.getSinglePaginationButton("&raquo;", () => loadPage(pageInfo.pageCount - 1)));
            }
        } catch (error) {
            errorHandler.handleError(error, this.paginationConstants.paginationErrorMessage, true);
        }
    }

    /**
     * Creates and returns a single dom element PaginationButton.
     * @param {*} innerHTML 
     * @param {*} loadPage function of type () => {}
     * @param {*} active boolean
     * @returns 
     */
    getSinglePaginationButton(innerHTML, loadPage, active = false) {
        const paginationButton = document.createElement("button");
        paginationButton.onclick = loadPage;
        paginationButton.innerHTML = innerHTML;
        if (active) {
            paginationButton.classList.add(this.paginationConstants.activeClass);
        }
        return paginationButton;
    }
}

export default new PaginationManager();