import * as constants from "../constants.js";
import errorHandler from "../generalTools/errorHandler.js";
import htmlHelper from "../generalTools/htmlHelper.js";
import requestHelper from "../generalTools/requestHelper.js";
import modalDialogHelper from "../generalTools/modalDialogHelper.js";
import accountManager from "./accountManager.js";
import paginationManager from "./paginationManager.js";
import Observable from "../generalTools/observable.js";

class OrderManager extends Observable {
    constructor() {
        super();

        this.orderConstants = {
            loadingErrorMessage: "Error loading the order list.",
            loginNeededMessage: "Pro zobrazení objednávek je potřeba přihlášení.",
            emptyListMessage: "Nemáte žádné objednávky",
            deletingErrorMessage: "Error deleting an order.",
            orderDeleteSuccessfulMessage: "Objednávka byla úspěšně smazána.",

            deleteModalDialogId: "deleteModalDialog",
            deleteModalDialogNoButtonId: "deleteModalDialogNoButton",
            deleteModalDialogYesButtonId: "deleteModalDialogYesButton",

            defaultPageIndex: 0,
            defaultPageSize: 5
        };

        this.ordersTab = document.getElementById(constants.ordersTabId);
        this.img = new Image();
        this.img.src = "fe/resources/img/packageIcon.png";

        this.loadParams = {
            sort: "NEW",
            pageIndex: this.orderConstants.defaultPageIndex,
            pageSize: this.orderConstants.defaultPageSize
        };
    }

    async setEvents() {
        this.setBinEvents();
    }

    /**
     * Loads order list from BE using XMLHttpRequest
     */
    async loadOrderList(loadParams) {
        try {
            // Update loadParams if necessary
            if (loadParams?.sort !== undefined && loadParams?.sort !== null) {
                this.loadParams.sort = loadParams.sort;
            }
            if (loadParams?.pageIndex !== undefined && loadParams?.pageIndex !== null) {
                this.loadParams.pageIndex = loadParams.pageIndex;
            }
            if (loadParams?.pageSize !== undefined && loadParams?.pageSize !== null) {
                this.loadParams.pageSize = loadParams.pageSize;
            }

            // Check login
            if (accountManager.authentication) {
                // Prepare and send request
                const request = new XMLHttpRequest();
                const params = requestHelper.getQueryParamString({...this.loadParams});
                request.open("GET", `${constants.getOrderListURL}?${params}`, true);
                request.setRequestHeader(accountManager.accountConstants.authorizationCookieName, `Bearer ${accountManager.bearerToken}`);
                request.onload = () => this.handleOrderListLoaded.bind(this)(request, loadParams?.witholdObserverNotification);
                request.onerror = errorHandler.handleRequestError;
                request.ontimeout = errorHandler.getRequestTimeoutHandler("Loading the order list timed out.");
                request.timeout = constants.defaultRequestTimeout;
                request.send();
            } else {
                this.clearTab();

                this.showEmptyOrderList(this.orderConstants.loginNeededMessage);

                // Hide pagination
                paginationManager.setEmptyPagination();
            }
        } catch (error) {
            errorHandler.handleError(error, this.orderConstants.loadingErrorMessage, true);
        }
    }

    /**
     * Parses response json and displays the results.
     * @param {*} request
     */
    handleOrderListLoaded(request, witholdObserverNotification) {
        const response = JSON.parse(request.responseText);

        if (!requestHelper.isOk(response)) {
            errorHandler.handleError(response.error, `${this.orderConstants.loadingErrorMessage}\nMessage: ${response.error.message}`, true);
            return;
        }

        // Display orders
        this.displayOrders(response.orderList);
        
        // Display pagination
        paginationManager.setPagination(
            response.pageInfo,
            (pageIndex) => this.loadOrderList({pageIndex})
        );

        // Notify observers
        if (!witholdObserverNotification) {
            let observerData = {
                tabButtonId : constants.ordersTabButtonId,
                tabId: constants.ordersTabId,
                loadParams: Object.assign({}, this.loadParams)
            }
            observerData.loadParams.witholdObserverNotification = true;
            this.notifyObservers(observerData);
        }
    }

    /**
     * Clears previous data and displays the new order list.
     * @param {*} orderList 
     */
    displayOrders(orderList) {
        this.clearTab();
        if (orderList.length) {
            orderList.forEach(order => this.addSingleOrder(order));
        } else {
            this.showEmptyOrderList(this.orderConstants.emptyListMessage);
        }
    }

    /**
     * Creates new orderTileContainer and orderTile.
     * Adds them to the ordersTab.
     */
    addSingleOrder(order) {
        const orderId = this.getHtmlOrderId(order);
        const escapedOrder = htmlHelper.escapeHTMLObjectValues(order);

        const orderHtml = `
        <div class="${constants.orderTileContainerClass}">
            <div id="${orderId}" class="${constants.orderTileClass}">
                <div>${escapedOrder.name}</div>
                <div>počet: ${escapedOrder.productCount}</div>
                <div>cena: ${String(escapedOrder.orderPrice).replace(".", ",")} ${escapedOrder.currency}</div>
            </div>
        </div>
        `;
        const orderElement = htmlHelper.createElementFromString(orderHtml);
        
        const orderTile = orderElement.querySelector(`#${orderId}`);
        orderTile.draggable = true;
        orderTile.ondragstart = (ev) => {
            ev.dataTransfer.setData("id", order.id);
            ev.dataTransfer.setData("name", escapedOrder.name);
            ev.dataTransfer.setDragImage(this.img, 10, 10);
        };

        this.ordersTab.append(orderElement);
    }

    /**
     * Sets binSvg drag events
     */
    setBinEvents() {
        const bin = document.getElementById(constants.binSvgId);
        bin.ondragover = (ev) => { ev.preventDefault(); };
        bin.ondrop = (ev) => {
            this.openDeleteModalDialog.bind(this)(ev);
            dragCounter = 0;
            this.setBinIcon(false);
        };

        // Child elements trigger the same ondragenter and ondragleave so we need a counter to to determine the main ondragleave
        let dragCounter = 0;
        // Move the lid up
        bin.ondragenter = (ev) => {
            dragCounter++;
            this.setBinIcon(true);
        };
        // Move the lid down
        bin.ondragleave = (ev) => {
            dragCounter--;
            if (dragCounter) return;
            this.setBinIcon(false);
        };
    }

    /**
     * Sets opened/closed svg icon
     * @param {*} open 
     */
    setBinIcon(open = false) {
        const lidHandle = document.getElementById(constants.binLidHandleId);
        const lidBody = document.getElementById(constants.binLidBodyId);

        if (open) {
            lidHandle.setAttribute("y", 0);
            lidBody.setAttribute("points", "18,10 82,10 85,25 15,25");
        } else {
            lidHandle.setAttribute("y", 10);
            lidBody.setAttribute("points", "18,20 82,20 85,35 15,35");
        }
    }

    /**
     * Open delete order confirmation modal dialog.
     * @param {*} ev 
     */
    openDeleteModalDialog(ev) {
        ev.preventDefault();
        const id = ev.dataTransfer.getData("id");
        const name = ev.dataTransfer.getData("name");

        const deleteHeader = "Zrušit objednávku";
        const deleteHtml = `
            <div id="${this.orderConstants.deleteModalDialogId}">
                <div class="modalRow">
                    Opravdu si přejete zrušit objednávku: "${name}"?
                </div>
                <div class="modalRow modalRowSubmit">
                    <div class="buttonArea">
                        <button id="${this.orderConstants.deleteModalDialogNoButtonId}" class="modalRowSubmitButton" title="Ne">
                            <span>Ne</span>
                        </button>
                        <button id="${this.orderConstants.deleteModalDialogYesButtonId}" class="modalRowSubmitButton" title="Ano">
                            <span>Ano</span>
                        </button>
                    </div>
                </div>
            </div>
        `;
        const deleteModalDialog = htmlHelper.createElementFromString(deleteHtml);

        const deleteModalDialogNoButton = deleteModalDialog.querySelector(`#${this.orderConstants.deleteModalDialogNoButtonId}`);
        deleteModalDialogNoButton.onclick = (event) => modalDialogHelper.closeModalDialog();

        const deleteModalDialogYesButton = deleteModalDialog.querySelector(`#${this.orderConstants.deleteModalDialogYesButtonId}`);
        deleteModalDialogYesButton.onclick = (event) => this.deleteOrder.bind(this)(id);

        modalDialogHelper.openModalDialog(deleteHeader, deleteModalDialog);
    }

    /**
     * Calls BE to delete an order
     * @param {*} id 
     */
    async deleteOrder(id) {
        try {
            const request = new XMLHttpRequest();
            const params = requestHelper.getQueryParamString({id});
            request.open("POST", `${constants.deleteOrderURL}?${params}`, true);
            request.setRequestHeader(accountManager.accountConstants.authorizationCookieName, `Bearer ${accountManager.bearerToken}`);
            request.onload = () => this.handleDeleteOrderLoaded.bind(this)(request);
            request.onerror = errorHandler.handleRequestError;
            request.ontimeout = errorHandler.getRequestTimeoutHandler("Deleting the order timed out.");
            request.timeout = constants.defaultRequestTimeout;
            request.send();
        } catch (error) {
            errorHandler.handleError(error, this.orderConstants.deletingErrorMessage, true);
        }
    }

    /**
     * Handle response from BE after deleting an order
     * @param {*} request 
     * @returns 
     */
    async handleDeleteOrderLoaded(request) {
        try {
            const response = JSON.parse(request.responseText);

            // Handle response errors
            if (!requestHelper.isOk(response)) {
                // Handle general be errors.
                errorHandler.handleError(response.error, `${this.orderConstants.deletingErrorMessage}\nMessage: ${response.error.message}`, true);
                return;
            }

            // Reload page
            await this.loadOrderList();

            // Show success
            modalDialogHelper.openModalDialog(this.orderConstants.orderDeleteSuccessfulMessage, null,true);
        } catch (error) {
            errorHandler.handleError(error, this.orderConstants.deletingErrorMessage, true);
        }
    }

    /**
     * If the user is not logged in,
     * then this function is called to show the message to the user.
     * @param {*} messageElement 
     */
     showEmptyOrderList(message, messageElement) {
        const messageContainer = document.createElement("div");
        messageContainer.classList.add(constants.nothingFoundClass);
        if (message) {
            messageContainer.innerHTML = message;
        } else if (messageElement) {
            messageContainer.append(messageElement);
        }
        this.ordersTab.append(messageContainer);
    }

    /**
     * Clears all the children of orders tab.
     */
    clearTab() {
        htmlHelper.clearChildren(this.ordersTab);
    }

    /**
     * Return id for / of html element.
     * @param {*} order
     * @returns string
     */
    getHtmlOrderId(order) {
        return `OrderId_${order.id}`;
    }
}

export default new OrderManager();