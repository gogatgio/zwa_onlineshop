import * as constants from "../constants.js";
import errorHandler from "../generalTools/errorHandler.js";
import htmlHelper from "../generalTools/htmlHelper.js";
import requestHelper from "../generalTools/requestHelper.js";
import modalDialogHelper from "../generalTools/modalDialogHelper.js";
import formHelper from "../generalTools/formHelper.js";
import Validator from "../generalTools/validator.js";
import { zwaEmail, zwaPassword } from "../generalTools/baseValidations.js";
import cookieHelper from "../generalTools/cookieHelper.js";
import zwaErrorCodes from "../generalTools/zwaErrorCodes.js";
import dateHelper from "../generalTools/dateHelper.js";

class accountManager {
    constructor() {
        this.accountConstants = {
            authorizationCookieName: "Authorization",

            // login constants
            loginFormId: "loginForm",
            loginEmailId: "loginEmail",
            loginPasswordId: "loginPassword",
            loginSubmitButtonId: "loginSubmitButton",
            loginErrorMessage: "Error loggin in.",

            // auto login constants
            autoLoginErrorMessage: "Error auto loggin in.",

            // register constants
            registerFormId: "registerForm",
            registerEmailId: "registerEmail",
            registerPassword1Id: "registerPassword1",
            registerPassword2Id: "registerPassword2",
            registerSubmitButtonId: "registerSubmitButton",
            registerErrorMessage: "Error registering.",

            // logout constants
            logoutErrorMessage: "Error loggin out."
        };

        this.authentication = null;
        this.bearerToken = null;
        this.handleLoginLogoutFinished = null;
    }

    /**
     * Opens login modal dialog and sets all the eventListeners.
     */
    async openLoginModalDialog() {
        try {
            const loginHeader = "Přihlášení";
            const loginHtml = `
                <form id="${this.accountConstants.loginFormId}" novalidate>
                    <div class="modalRow">
                        ${formHelper.createZwaInput(this.accountConstants.loginEmailId, "Email*")}
                    </div>
                    <div class="modalRow">
                        ${formHelper.createZwaInput(this.accountConstants.loginPasswordId, "Heslo*", "password")}
                    </div>
                    <div class="modalRow modalRowSubmit">
                        <button id="${this.accountConstants.loginSubmitButtonId}" class="modalRowSubmitButton" title="Přihlásit">
                            <img src="fe/resources/img/loginIcon.svg" alt="loginIcon">
                            <span>Přihlásit</span>
                        </button>
                    </div>
                </form>
            `;
            const loginForm = htmlHelper.createElementFromString(loginHtml);

            const loginValidationTypes = {
                [this.accountConstants.loginEmailId]: new zwaEmail(true),
                [this.accountConstants.loginPasswordId]: new zwaPassword()
            }

            const loginValidator = new Validator(loginForm, loginValidationTypes);

            const loginSubmitButton = loginForm.querySelector(`#${this.accountConstants.loginSubmitButtonId}`);
            loginSubmitButton.onclick = (event) => this.handleLoginSubmitButtonClick(event, loginValidator);

            modalDialogHelper.openModalDialog(loginHeader, loginForm);
        } catch (error) {
            errorHandler.handleError(error, null, true);
        }
    }

    /**
     * Opens register modal dialog and sets all the eventListeners.
     */
    async openRegisterModalDialog() {
        try {
            const registerHeader = "Registrace";
            const registerHtml = `
                <form id="${this.accountConstants.registerFormId}">
                    <div class="modalRow">
                        ${formHelper.createZwaInput(this.accountConstants.registerEmailId, "Email*")}
                    </div>
                    <div class="modalRow">
                        ${formHelper.createZwaInput(this.accountConstants.registerPassword1Id, "Heslo*", "password")}
                    </div>
                    <div class="modalRow">
                        ${formHelper.createZwaInput(this.accountConstants.registerPassword2Id, "Heslo znovu*", "password")}
                    </div>
                    <div class="modalRow modalRowSubmit">
                        <button id="${this.accountConstants.registerSubmitButtonId}" class="modalRowSubmitButton" title="Registrovat">
                            <img src="fe/resources/img/registerIcon.svg" alt="registerIcon">
                            <span>Registrovat</span>
                        </button>
                    </div>
                </form>
            `;
            const registerForm = htmlHelper.createElementFromString(registerHtml);

            const passwordsEqualValidation = (value) => {
                const errors = [];
                const password1 = registerForm.querySelector(`#${this.accountConstants.registerPassword1Id}`)?.value;
                if (value !== password1) {
                    errors.push("Heslo musí být stejné.");
                }
                return errors;
            }
            const registerValidationTypes = {
                [this.accountConstants.registerEmailId]: new zwaEmail(true),
                [this.accountConstants.registerPassword1Id]: new zwaPassword(),
                [this.accountConstants.registerPassword2Id]: new zwaPassword(passwordsEqualValidation)
            }

            const registerValidator = new Validator(registerForm, registerValidationTypes);

            const registerSubmitButton = registerForm.querySelector(`#${this.accountConstants.registerSubmitButtonId}`);
            registerSubmitButton.onclick = (event) => this.handleRegisterSubmitButtonClick(event, registerValidator);

            modalDialogHelper.openModalDialog(registerHeader, registerForm);
        } catch (error) {
            errorHandler.handleError(error, null, true);
        }
    }

    /**
     * Validates the login form and sends the XMLHttpRequest to the BE.
     * @param {*} event 
     * @param {*} loginValidator 
     */
    async handleLoginSubmitButtonClick(event, loginValidator) {
        try {
            event.preventDefault();

            // Check form validity
            if (!loginValidator.validateForm()) {
                return;
            }

            // Prepare login data
            const loginEmail = document.getElementById(this.accountConstants.loginEmailId).value;
            const loginPassword = document.getElementById(this.accountConstants.loginPasswordId).value;

            const loginData = JSON.stringify({
                [this.accountConstants.loginEmailId]: loginEmail,
                [this.accountConstants.loginPasswordId]: loginPassword
            });

            // Send login data to BE
            const request = new XMLHttpRequest();
            request.open("POST", constants.loginURL, true);
            request.setRequestHeader('Content-Type', 'application/json');
            request.onload = () => this.handleLoginLoaded.bind(this)(request, loginValidator);
            request.onerror = (error) => console.warn(`${this.accountConstants.loginErrorMessage} Message: ${JSON.stringify(error, null, 2)}`);
            request.ontimeout = (progressEvent) => console.warn("Sending login data to BE timed out.");
            request.timeout = constants.defaultRequestTimeout;
            request.send(loginData);
        } catch (error) {
            errorHandler.handleError(error, "Error submitting the login info.", true);
        }
    }

    async handleLoginLoaded(request, loginValidator) {
        try {

            const response = JSON.parse(request.responseText);

            // Handle response errors
            if (!requestHelper.isOk(response)) {
                if (requestHelper.containsValidationErrors(response)) {
                    // Handle form validation results
                    loginValidator.showBeValidationResults(response.error.validationResults);
                } else {
                    // Handle general be errors.
                    errorHandler.handleError(response.error, `${this.accountConstants.loginErrorMessage}\nMessage: ${response.error.message}`, true);
                }
                return;
            }

            // Set authentication
            // hide buttons / show email
            await this.login(response.bearerToken);

            //Close login modal dialog
            modalDialogHelper.closeModalDialog();
        } catch (error) {
            errorHandler.handleError(error, this.accountConstants.loginErrorMessage, true);
        }
    }

    /**
     * Validates the register form and sends the XMLHttpRequest to the BE.
     * @param {*} event 
     * @param {*} registerValidator 
     */
    async handleRegisterSubmitButtonClick(event, registerValidator) {
        try {
            event.preventDefault();

            // Check form validity
            if (!registerValidator.validateForm()) {
                return;
            }

            // Prepare register data
            const registerEmail = document.getElementById(this.accountConstants.registerEmailId).value;
            const registerPassword1 = document.getElementById(this.accountConstants.registerPassword1Id).value;
            const registerPassword2 = document.getElementById(this.accountConstants.registerPassword2Id).value;

            const registerData = JSON.stringify({
                [this.accountConstants.registerEmailId]: registerEmail,
                [this.accountConstants.registerPassword1Id]: registerPassword1,
                [this.accountConstants.registerPassword2Id]: registerPassword2
            });

            // Send login data to BE
            const request = new XMLHttpRequest();
            request.open("POST", constants.registerURL, true);
            request.setRequestHeader('Content-Type', 'application/json');
            request.onload = () => this.handleRegisterLoaded.bind(this)(request, registerValidator);
            request.onerror = (error) => console.warn(`${this.accountConstants.registerErrorMessage} Message: ${JSON.stringify(error, null, 2)}`);
            request.ontimeout = (progressEvent) => console.warn("Sending register data to BE timed out.");
            request.timeout = constants.defaultRequestTimeout;
            request.send(registerData);
        } catch (error) {
            errorHandler.handleError(error, "Error submitting the register info.", true);
        }
    }

    async handleRegisterLoaded(request, registerValidator) {
        try {

            const response = JSON.parse(request.responseText);

            // Handle response errors
            if (!requestHelper.isOk(response)) {
                if (requestHelper.containsValidationErrors(response)) {
                    // Handle form validation results
                    registerValidator.showBeValidationResults(response.error.validationResults);
                } else {
                    // Handle general be errors.
                    errorHandler.handleError(response.error, `${this.accountConstants.registerErrorMessage}\nMessage: ${response.error.message}`, true);
                }
                return;
            }

            // Set authentication
            // hide buttons / show email
            await this.login(response.bearerToken);

            //Close register modal dialog
            modalDialogHelper.closeModalDialog();
        } catch (error) {
            errorHandler.handleError(error, this.accountConstants.registerErrorMessage, true);
        }
    }

    /**
     * Tries wheter there is a saved authorizationCookie cookie
     * and whetherit is still valid.
     */
    async tryAutoLogin() {
        try {
            // Check bearer token on FE
            const authorizationCookie = await cookieHelper.getCookieOrDefault(this.accountConstants.authorizationCookieName);
        
            if (!authorizationCookie) {
                return;
            }
    
            const bearerToken = authorizationCookie.split(" ")[1];
            if (!bearerToken) {
                return;
            }

            // Check bearer token on BE
            const request = new XMLHttpRequest();
            request.open("POST", constants.checkLoginURL, true);
            request.setRequestHeader(this.accountConstants.authorizationCookieName, `Bearer ${bearerToken}`);
            request.onload = () => this.handleCheckLoginLoaded.bind(this)(request, bearerToken);
            request.onerror = (error) => console.warn(`${this.accountConstants.autoLoginErrorMessage} Message: ${JSON.stringify(error, null, 2)}`);
            request.ontimeout = (progressEvent) => console.warn("Checking login with BE timed out.");
            request.timeout = constants.defaultRequestTimeout;
            request.send();
        } catch (error) {
            errorHandler.handleError(error, this.accountConstants.autoLoginErrorMessage, true);
        }
    }

    async handleCheckLoginLoaded(request, bearerToken) {
        try {
            const response = JSON.parse(request.responseText);

            // Handle response errors
            if (!requestHelper.isOk(response)) {
                // We can ignore expired or invalid tokens and just not log in the user.
                if (!this.isExpiredOrInvalidTokenError(response.error)) {
                    // Handle general be errors.
                    errorHandler.handleError(response.error, `${this.accountConstants.autoLoginErrorMessage}\nMessage: ${response.error.message}`, true);
                }
                // Delete save token and properties by calling logout function
                this.logout();
                return;
            }

            await this.login(bearerToken, false);
        } catch (error) {
            errorHandler.handleError(error, this.accountConstants.autoLoginErrorMessage, true);
        }
    } 

    /**
     * Sets the Authorization Bearer token cookie.
     * Decodes the bearer token.
     * @param {*} bearerToken 
     */
    async login(bearerToken, setCookie = true) {
        this.bearerToken = bearerToken;
        
        // Decode bearer token payload
        const encodedJwtPayload = bearerToken.split(".")[1];
        const decodedJwtPayload = this.base64UrlDecode(encodedJwtPayload);
        const jwtPayloadObject = JSON.parse(decodedJwtPayload);
        this.authentication = jwtPayloadObject;

        // Save the bearer token to a cookie
        if (setCookie) {
            let expirationDate  = null;
            if (this.authentication?.expiration?.date) {
                expirationDate = dateHelper.convertUtcDateToLocal(new Date(this.authentication?.expiration?.date));
            }
            await cookieHelper.setCookie(this.accountConstants.authorizationCookieName, `Bearer ${bearerToken}`, expirationDate);
        }

        // Hide / show account control buttons
        await this.showOrHideAccountControls();

        // Handle login / logout tasks
        if (this.handleLoginLogoutFinished && typeof this.handleLoginLogoutFinished === "function") {
            await this.handleLoginLogoutFinished();
        }
    }

    /**
     * Sends the XMLHttpRequest to the BE
     * and then hides / shows the appropriate page elements.
     * @param {*} event 
     * @param {*} registerValidator 
     */
    async logout() {
        try {
            const request = new XMLHttpRequest();
            request.open("POST", constants.logoutURL, true);
            request.setRequestHeader(this.accountConstants.authorizationCookieName, `Bearer ${this.bearerToken}`);
            request.onload = () => this.handleLogoutLoaded.bind(this)(request);
            request.onerror = (error) => console.warn(`${this.accountConstants.logoutErrorMessage} Message: ${JSON.stringify(error, null, 2)}`);
            request.ontimeout = (progressEvent) => console.warn("Sending logout request to BE timed out.");
            request.timeout = constants.defaultRequestTimeout;
            request.send();
        } catch (error) {
            errorHandler.handleError(error, this.accountConstants.logoutErrorMessage, true);
        }
    }

    async handleLogoutLoaded(request) {
        try {

            const response = JSON.parse(request.responseText);

            // Handle response errors
            if (!requestHelper.isOk(response) && !this.isExpiredOrInvalidTokenError(response.error)) {
                // Handle general be errors.
                errorHandler.handleError(response.error, `${this.accountConstants.logoutErrorMessage}\nMessage: ${response.error.message}`, true);
                return;
            }

            // Set account properties to null
            this.bearerToken = null;
            this.authentication = null;

            // Delete authorization cookie containing the bearer token.
            await cookieHelper.deleteCookie(this.accountConstants.authorizationCookieName);

            // Hide / show account control buttons
            await this.showOrHideAccountControls();

            // Handle login / logout tasks
            if (this.handleLoginLogoutFinished && typeof this.handleLoginLogoutFinished === "function") {
                await this.handleLoginLogoutFinished();
            }
        } catch (error) {
            errorHandler.handleError(error, this.accountConstants.logoutErrorMessage, true);
        }
    }
    
    /**
     * Checks accountManager authentication property and sets elements styles based on it.
     */
    showOrHideAccountControls() {
        const loggedOutElemetsDisplayStyle = this.authentication ? "none" : "flex";
        const loggedInElemetsDisplayStyle = this.authentication ? "flex" : "none";

        const loggedOutControls = document.getElementById(constants.loggedOutControlsId);
        loggedOutControls.style.display = loggedOutElemetsDisplayStyle;

        const loggedInControls = document.getElementById(constants.loggedInControlsId);
        loggedInControls.style.display = loggedInElemetsDisplayStyle;

        const accountEmail = document.getElementById(constants.accountEmailId);
        accountEmail.innerText = this.authentication?.email;
    }

    /**
     * Returns true if we can ignore these BE errors during logout request.
     * @param {*} error 
     * @returns 
     */
    isExpiredOrInvalidTokenError(error) {
        if (error.zwaErrorCode) {
            if (error.zwaErrorCode === zwaErrorCodes.tokenExpired
                || error.zwaErrorCode === zwaErrorCodes.tokenInvalid) {
                    return true;
                }
        }

        return false;
    }

    /**
     * Replaces url special chars and decodes base64 string.
     * @param {*} string string to be decoded
     * @returns string
     */
    base64UrlDecode(string) {
        return atob(string.replace(/-/g, "+").replace(/_/g, "/"));
    }

    /**
     * Returns current account authentication profiles
     * @returns array of strings | null
     */
    getAccountProfiles() {
        return this.authentication?.profiles;
    }
}

export default new accountManager;