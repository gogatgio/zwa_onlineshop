const environment = "local"; // toad, local
export const minLogLevel = "warning"; // error, warning, information, debug, trace

export default environment;