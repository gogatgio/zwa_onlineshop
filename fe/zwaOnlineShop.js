import * as constants from "./constants.js";
import errorHandler from "./generalTools/errorHandler.js";
import logger from "./generalTools/logger.js";
import logLevels from "./generalTools/logLevels.js";
import themeManager from "./pageManagers/themeManager.js";
import productManager from "./pageManagers/productManager.js";
import aboutManager from "./pageManagers/aboutManager.js";
import modalDialogHelper from "./generalTools/modalDialogHelper.js";
import accountManager from "./pageManagers/accountManager.js";
import profileCodes from "./generalTools/profileCodes.js";
import htmlHelper from "./generalTools/htmlHelper.js";
import orderManager from "./pageManagers/orderManager.js";

let activeTab = "";

window.onload = handlePageLoad;

/**
 * This function is called after the page has been loaded.
 * Loads data and sets up the page style and state.
 */
async function handlePageLoad() {
    try {
        themeManager.loadThemeFromLocalStorage();
        accountManager.handleLoginLogoutFinished = handleLoginLogoutFinished;
        accountManager.tryAutoLogin();
        await setHistoryHandeling();
        await setButtonOnClickEvents();
        // openDeafultTab() uses click and history so we need to await previous functions
        openDeafultTab();
        setUpOfflineIndicator();
    } catch (error) {
        errorHandler.handleError(error, "Error setting up page during loading.", true);
    }
}

/**
 * Sets onClick events for all the buttons.
 */
async function setButtonOnClickEvents() {
    try {
        setSingleButtonOnClickEvent(constants.loginButtonId, accountManager.openLoginModalDialog.bind(accountManager));
        setSingleButtonOnClickEvent(constants.logoutButtonId, accountManager.logout.bind(accountManager));
        setSingleButtonOnClickEvent(constants.registerButtonId, accountManager.openRegisterModalDialog.bind(accountManager));
        setSingleButtonOnClickEvent(constants.productsTabButtonId, () => openTab(constants.productsTabButtonId, constants.productsTabId));
        setSingleButtonOnClickEvent(constants.ordersTabButtonId, () => openTab(constants.ordersTabButtonId, constants.ordersTabId));
        setSingleButtonOnClickEvent(constants.addProductButtonId, productManager.addProduct.bind(productManager));
        setSingleButtonOnClickEvent(constants.sortButtonId, (event) => productManager.sort.bind(productManager)(event, productManager.productConstants.sortOptions.new));
        setSingleButtonOnClickEvent(constants.searchButtonId, (event) => productManager.search(event));
        setSingleButtonOnClickEvent(constants.themes.blueThemeId, () => themeManager.changeTheme(constants.themes.blueThemeId));
        setSingleButtonOnClickEvent(constants.themes.greenThemeId, () => themeManager.changeTheme(constants.themes.greenThemeId));
        setSingleButtonOnClickEvent(constants.themes.redThemeId, () => themeManager.changeTheme(constants.themes.redThemeId));
        setSingleButtonOnClickEvent(constants.aboutButtonId, aboutManager.openAboutModalDialog.bind(aboutManager));
        setSingleButtonOnClickEvent(constants.audioButtonId, handleAudioButtonClick);
        setSingleButtonOnClickEvent(constants.closeModalDialogButtonId, modalDialogHelper.closeModalDialog.bind(modalDialogHelper));

        await orderManager.setEvents();
    } catch(error) {
        errorHandler.handleError(error, "Error setting button onClick events.", true);
    }
}

/**
 * Sets the onClick event for the button.
 * @param {*} buttonId 
 * @param {*} event 
 */
function setSingleButtonOnClickEvent(buttonId, onClickEvent) {
    const button = document.getElementById(buttonId);
    if (button) {
        button.onclick = onClickEvent;
    } else {
        logger.log(`Could not set onClickEvent for buttonId: ${buttonId}`, logLevels.warning);
    }
}

/**
 * Opens the deafault tab after page loads.
 */
async function openDeafultTab() {
    try {
        const {
            productsTabButtonId,
            productsTabId
        } = constants.defaultTab;
        openTab(productsTabButtonId, productsTabId);
    } catch (error) {
        errorHandler.handleError(error, `Could not open the default tab: ${constants?.defaultTab}`)
    }
}

/**
 * Handles clicks on tabLink.
 * Returns true if a new tab was opened.
 * Returs false if no new tab was opened or there was an error.
 */
async function openTab(tabButtonId, tabId, loadParams) {
    try {
        let targetTab = document.getElementById(tabId);

        // check active tab
        if (targetTab === activeTab && !loadParams) {
            return false;
        } else {
            activeTab = targetTab;
        }
    
        // Get all elements with class="tabContent" and hide them
        let tabcontent = document.getElementsByClassName(constants.tabContentClass);
        for (let i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
    
        // Get all elements with class="tabLinks" and remove the class "active"
        let tablinks = document.getElementsByClassName(constants.tabLinkClass);
        for (let i = 0; i < tablinks.length; i++) {
            tablinks[i].classList.remove(constants.activeTabClass);
        }
    
        // Show the current tab, and add an "active" class to the button that opened the tab
        targetTab.style.display = "flex";
        document.getElementById(tabButtonId).classList.add(constants.activeTabClass);
        switch (activeTab.id) {
            case constants.productsTabId:
                productManager.loadProductList(loadParams);
                break;
            case constants.ordersTabId:
                orderManager.loadOrderList(loadParams);
                break;
        }
    
        // Show / Hide buttons
        htmlHelper.showOrHideElement(constants.addProductButtonId, showAddProductButton());
        htmlHelper.showOrHideElement(constants.sortButtonId, activeTab.id === constants.productsTabId);
        htmlHelper.showOrHideElement(constants.searchFormId, activeTab.id === constants.productsTabId);

        htmlHelper.showOrHideElement(constants.binSvgId, activeTab.id === constants.ordersTabId);

        return true;
    } catch (error) {
        errorHandler.handleError(error, `Could not open tab ${tabId}`, true);
        return false;
    }
}

/**
 * Perform some tasks after login / logout.
 * Show / display some elements.
 */
async function handleLoginLogoutFinished() {
    try {
        // Show / Hide addProductButton
        htmlHelper.showOrHideElement(constants.addProductButtonId, showAddProductButton());
        // Reload tab data
        switch (activeTab.id) {
            case constants.productsTabId:
                productManager.reloadCurrentProductDetail();
                break;
            case constants.ordersTabId:
                orderManager.loadOrderList();
                break;
        }
    } catch (error) {
        errorHandler.handleError(error, "Error handling tasks after login / logout.", true);
    }
}

/**
 * Checks whether addProductButton should be displayed.
 * @returns boolean
 */
function showAddProductButton() {
    // User must be logged in
    if (!accountManager.authentication) {
        return false;
    }

    // User must be a manager
    const accountProfiles = accountManager.getAccountProfiles();
    if (!accountProfiles || !accountProfiles.includes(profileCodes.manager)) {
        return false;
    }

    // ActiveTab must be productsTab
    if (activeTab.id !== constants.productsTabId) {
        return false;
    }

    return true;
}

// Play / pause background audio
async function handleAudioButtonClick() {
    const audio = document.getElementById(constants.backgroundAudioId);
    audio.paused ? audio.play() : audio.pause();

    // Corss / uncross the icon
    const diagonalLine = document.getElementById(constants.audioButtonCrossId);
    diagonalLine.classList.toggle("hidden");
}

// First history needs to replace the current state while loading the default tab.
var firstHistorySet = false;
async function setNewHistory(data) {
    firstHistorySet ?
    history.pushState(data, '') :
    history.replaceState(data, '');

    firstHistorySet = true;
}

/**
 * Sets listeners to handle browser window history changes.
 */
async function setHistoryHandeling() {
    window.addEventListener('popstate', () => {
        const state = history.state;
        if (state)
            openTab(state.tabButtonId, state.tabId, state.loadParams);
    });

    let historyObserver = {
        updateObserver(data) {
            setNewHistory(data);
        }
    }

    productManager.attachObserver(historyObserver)
    orderManager.attachObserver(historyObserver);
}

/**
 * Sets window event listeners to handle offline/online status change.
 */
function setUpOfflineIndicator() {
    const offlineIndicator = document.getElementById(constants.offlineIndicatorId);
    
    const updateOfflineIndicator = () => offlineIndicator.classList.toggle("hidden", navigator.onLine);

    // We need to check when the window loads as well.
    updateOfflineIndicator();
    
    window.addEventListener("offline", updateOfflineIndicator);
    window.addEventListener("online", updateOfflineIndicator);
}