import * as constants from "../constants.js";
import errorHandler from "../generalTools/errorHandler.js";
import requestHelper from "../generalTools/requestHelper.js";

class locationHelper {
    constructor() {
        this.getLocation();
    }

    /**
     * Returns true if the user is in Czech.
     * Returns false if the user is elsewhere or has not allowed location.
     * @returns 
     */
    isCzech() {
        return this?.positionInfo?.address?.country === "Czechia";
    }

    /**
     * Asks user to allow location.
     */
    getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(this.setPosition.bind(this), this.handleError);
        } else {
            console.warn("Geolocation is not supported by this browser.");
        }
    }

    /**
     * After the user allows location we call an external site to get more info about the position.
     * @param {*} position 
     */
    setPosition(position) {
        this.position = position;

        let loadParams = {
            format: "jsonv2",
            lat: this.position.coords.latitude,
            lon: this.position.coords.longitude
        }

        // Prepare and send request
        const request = new XMLHttpRequest();
        const params = requestHelper.getQueryParamString({...loadParams});
        request.open("GET", `${constants.locationURL}?${params}`, true);
        request.onload = () => this.handlePositionInfoLoaded.bind(this)(request);
        request.onerror = errorHandler.handleRequestError;
        request.ontimeout = errorHandler.getRequestTimeoutHandler("Loading the location timed out.");
        request.timeout = constants.defaultRequestTimeout;
        request.send();
    }

    /**
     * Handles position info loaded from external site.
     * @param {*} request 
     */
    handlePositionInfoLoaded(request) {
        this.positionInfo = JSON.parse(request.responseText);
    }

    /**
     * Handles error when calling navigator.geolocation.getCurrentPosition()
     * @param {*} error 
     */
    handleError(error) {
        console.warn(error);
    }
}

export default new locationHelper();
