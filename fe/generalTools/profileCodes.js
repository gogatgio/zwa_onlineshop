/**
 * Profile codes which are returned from BE in bearer tokens
 */
 const profileCodes = Object.freeze({
    basic: "BASIC",
    manager: "MANAGER"
});

export default profileCodes;