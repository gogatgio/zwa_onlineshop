import * as constants from "../constants.js";
import { minLogLevel } from "../config.js";
import logLevels from "./logLevels.js";
import requestHelper from "./requestHelper.js";

class logger {
    constructor() {
        this.errorMessage = "Error occured while sending a log to BE.";
    }

    /**
     * Checks wther logging is necessary for the logLevel and logs the message.
     * @param {*} message string to be logged
     * @param {*} logLevel error, warning, information, debug, trace
     * @param {*} stack optional string
     * @returns 
     */
    log(message, logLevel, stack = null) {
        try {
            // Check whether the logLevel is permitted
            if (!logLevel || logLevel.severity < logLevels[minLogLevel].severity) {
                return;
            }
            
            // Prepare the log data for the POST body
            const logData = JSON.stringify({
                utcdatetime: (new Date()).toISOString(),
                origin: "CLIENT",
                level: logLevel.name,
                message: message,
                stack: stack
            });

            // Send log data to BE
            const request = new XMLHttpRequest();
            request.open("POST", constants.logURL, true);
            request.setRequestHeader('Content-Type', 'application/json');
            request.onload = () => this.handleLogLoaded.bind(this)(request);
            request.onerror = (error) => console.warn(`${this.errorMessage} Message: ${JSON.stringify(error, null, 2)}`);
            request.ontimeout = (progressEvent) => console.warn("Sending a log to BE timed out.");
            request.timeout = constants.defaultRequestTimeout;
            request.send(logData);
        } catch (error) {
            console.warn(`${this.errorMessage} Message: ${error?.message}`);
        }
    }

    /**
     * 
     * @param {*} request 
     */
    handleLogLoaded(request) {
        const response = JSON.parse(request.responseText);
        if (!requestHelper.isOk(response)) {
            console.warn(`${this.errorMessage} Message: ${response.error.message}`);
        }
    }
}

export default new logger();