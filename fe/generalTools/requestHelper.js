class requestHelper {
    /**
     * Converts object entries to a query param string
     * @param {*} params object containing key value pairs that will be converted to a single string
     * @returns string
     */
    getQueryParamString(params) {
        return Object.entries(params)
            .map(([key, value]) => [key, value && value.toString().trim()]) // Value toString and trim
            .filter(([key, value]) => value) // Filter out empty params
            .map(([key, value]) => `${key}=${encodeURIComponent(value)}`) // Change param to query param string
            .join("&"); // Join all strings to a single one
    }

    /**
     * Checks whether a response contains any errors.
     * If there are any errors, FE returns them in the response json.
     * @param {*} request 
     * @returns 
     */
    isOk(response) {
        if (response.error) {
            return false;
        }

        return true;
    }

    /**
     * Checks whether a response contains any validation errors.
     * @param {*} request 
     * @returns 
     */
     containsValidationErrors(response) {
        if (response?.error?.validationResults) {
            return true;
        }

        return false;
    }
}

export default new requestHelper();