/**
 * Implements validate function for general data type.
 * Returns errors array.
 */
class zwaBaseValue {
    constructor (required) {
        this.required = required;
    }

    validate (value) {
        const errors = [];

        if (this.required && !value) {
            errors.push("Hodnota je povinná.");
        }

        return errors;
    }
}

/**
 * Implements validate function for int.
 * Returns errors array.
 */
 class zwaInt extends zwaBaseValue {
    constructor(required, min, max) {
        super(required);
        this.min = min;
        this.max = max;
    }

    validate(value) {
        const errors = [];

        const parentErrors = super.validate(value);
        if (parentErrors.length) {
            errors.push(...parentErrors);
            return errors;
        }

        const parsedInt = parseInt(value);
        if (!Number.isInteger(parsedInt) || value % 1 !== 0) {
            errors.push("Hodnota musí být celé číslo.");
            return errors;
        }

        if (this.min !== null && this.min !== undefined && parsedInt < this.min) {
            errors.push(`Číslo nesmí být menší než ${this.min}.`);
            return errors;
        }

        if (this.max && parsedInt > this.max) {
            errors.push(`Číslo nesmí být větší než ${this.max}.`);
            return errors;
        }

        return errors;
    }
}

/**
 * Implements validate function for string.
 * Returns errors array.
 */
class zwaString extends zwaBaseValue {
    constructor(required, minLength, maxLength) {
        super(required);
        this.minLength = minLength;
        this.maxLength = maxLength;
    }

    validate(value) {
        const errors = [];

        const parentErrors = super.validate(value);
        if (parentErrors.length) {
            errors.push(...parentErrors);
            return errors;
        }

        if (typeof value !== "string") {
            errors.push("Hodnota musí být text");
            return errors;
        }

        if (this.minLength && value.length < this.minLength) {
            errors.push(`Text nesmí být kratší než ${this.minLength} znaků.`);
            return errors;
        }

        if (this.maxLength && value.length > this.maxLength) {
            errors.push(`Text nesmí být delší než ${this.maxLength} znaků.`);
            return errors;
        }

        return errors;
    }
}

/**
 * Implements validate function for email.
 * Returns errors array.
 */
class zwaEmail extends zwaString {
    constructor(required) {
        super(required, 0, 50);
    }

    validate(value) {
        const errors = [];

        const parentErrors = super.validate(value);
        if (parentErrors.length) {
            errors.push(...parentErrors);
            return errors;
        }

        const emailFormat=/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/;
        if (!emailFormat.test(value)) {
            errors.push("Email není ve správném formátu.");
            return errors;
        }

        return errors;
    }
}

/**
 * Implements validate function for password.
 * Returns errors array.
 */
class zwaPassword extends zwaString {
    constructor(customValidation) {
        super(true, 6, 50);
        this.customValidation = customValidation;
    }

    validate(value) {
        const errors = [];

        if (this.customValidation) {
            const customErrors = this.customValidation(value);
            if (customErrors.length) {
                errors.push(...customErrors);
                return errors;
            }
        }

        const parentErrors = super.validate(value);
        if (parentErrors.length) {
            errors.push(...parentErrors);
            return errors;
        }

        if (value === value.toLowerCase()) {
            errors.push("Heslo musí obsahovat velké písmeno.");
            return errors;
        }

        if (value === value.toUpperCase()) {
            errors.push("Heslo musí obsahovat malé písmeno.");
            return errors;
        }

        if (value.search(/[0-9]/) < 0) {
            errors.push("Heslo musí obsahovat číslici.");
            return errors;
        }

        return errors;
    }
}

export { zwaInt, zwaString, zwaPassword, zwaEmail };