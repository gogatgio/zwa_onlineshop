/**
 * Custom error codes which are sent from BE.
 * FE generally handles these is a special way.
 */
const zwaErrorCodes = Object.freeze({
    tokenExpired: "TOKEN_EXPIRED",
    tokenInvalid: "TOKEN_INVALID"
});

export default zwaErrorCodes;