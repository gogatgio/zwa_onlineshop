import formHelper from "./formHelper.js";

class Validator {
    /**
     * Adds validation input event listener to all form inputs.
     * @param {*} form 
     * @param {*} validationTypes 
     */
    constructor(form, validationTypes) {
        this.form = form;
        this.validationTypes = validationTypes;
        this.addFormValidators();
    }

    /**
     * Adds validation input event listener to all form inputs.
     */
    addFormValidators() {
        Object.entries(this.validationTypes)
            .forEach(([inputId, inputValidation]) => this.addInputValidator(inputId, inputValidation));
    }

    /**
     * Adds input event listener that validates the input.
     * @param {*} inputId 
     * @param {*} inputValidation 
     */
    addInputValidator(inputId, inputValidation) {
        const input = this.getFormElement(inputId);
        input.addEventListener("input", (event) => this.validateInput(inputId, inputValidation));
    }

    /**
     * Validates single input.
     * @param {*} inputId 
     * @param {*} inputValidation 
     * @returns validity boolean.
     */
    validateInput(inputId, inputValidation) {
        const input = this.getFormElement(inputId);

        // Get input validation results
        const validationErrors = inputValidation.validate(input.value);
        const errorMessage = validationErrors.join(" ");

        // Set input validity (Cretes red border.)
        input.setCustomValidity(errorMessage);

        // Set input error message (Sets error message text under the input.)
        this.setInputErrorMessge(inputId, errorMessage);

        // If there are any errors, then return false.
        if (validationErrors.length) {
            return false;
        } else  {
            return true;
        }
    }

    /**
     * Sets error message text under the input.
     * @param {*} inputId 
     * @param {*} errorMessage 
     */
    setInputErrorMessge(inputId, errorMessage) {
        const errorMessageElement = this.getFormElement(formHelper.getErrorMessageId(inputId));
        if (!errorMessageElement) {
            return;
        }
        errorMessageElement.style.display = errorMessage ? "block" : "none";
        errorMessageElement.innerText = errorMessage;
    }

    /**
     * Returns dom elements that is in the form.
     * @param {*} inputId 
     * @returns 
     */
    getFormElement(inputId) {
        return this.form.querySelector(`#${inputId}`);
    }

    /**
     * Validates all form inputs and return the validity booloan.
     */
    validateForm() {
        let valid = true;

        Object.entries(this.validationTypes)
            .forEach(([inputId, inputValidation]) => {
                if (!this.validateInput(inputId, inputValidation)) {
                    valid = false;
                }
            });
        
        return valid;
    }

    /**
     * Sets input error messages based on the validation results comming from the be.
     * @param {*} validationResults 
     */
    showBeValidationResults(validationResults) {
        for (let validationResult of validationResults) {
            const inputId = validationResult.key;
            const errorMessage = validationResult.errors.join(" ");
            this.setInputErrorMessge(inputId, errorMessage);
        }
    }
}

export default Validator;