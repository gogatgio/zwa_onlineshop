class Observable {
    constructor() {
        this.observers = [];
    }

    attachObserver(observer) {
        this.observers.push(observer);
    }

    detachObserver(observer) {
        this.observers = this.observers.filter(o => o !== observer);
    }

    notifyObservers(data) {
        this.observers.forEach(o => o.updateObserver(data));
    }
}

export default Observable;