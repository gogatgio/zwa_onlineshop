const logLevels = Object.freeze({
    error: { name : "ERROR", severity : 4 },
    warning: { name : "WARNING", severity : 3 },
    information: { name : "INFORMATION", severity : 2 },
    debug: { name : "DEBUG", severity : 1 },
    trace: { name : "TRACE", severity : 0 },
});

export default logLevels;