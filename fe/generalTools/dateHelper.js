class dateHelper {
    /**
     * Converts object entries to a query param string
     * @param {*} params object containing key value pairs that will be converted to a single string
     * @returns string
     */
    convertUtcDateToLocal(date) {
        const utcDateMilliseconds = Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds());
        return new Date(utcDateMilliseconds);
    }
}

export default new dateHelper();