class htmlHelper {
    /**
     * Convert the value to a string and escapes html special characters.
     * Used before displaying the data from BE to client.
     * Prevents XSS.
     * @param {*} unsafeString 
     * @returns 
     */
    escapeHtmlString(unsafeString) {
        if (!unsafeString) {
            return unsafeString;
        }

        var replaceMap = {
            '&': '&amp;',
            '<': '&lt;',
            '>': '&gt;',
            '"': '&quot;',
            "'": '&#039;'
        };
        const escapedString = unsafeString.toString().replace(/[&<>"']/g, (char) => replaceMap[char]);
        return escapedString;
    }

    /**
     * Iterates through object entries and calls escapeHtmlString on every value.
     * Object values get converted to a string and special character replaced so that they can be displayed in html.
     * Prevents XSS.
     * @param {*} unsafeObject 
     * @returns 
     */
    escapeHTMLObjectValues(unsafeObject) {
        Object.entries(unsafeObject)
            .forEach(([key, value]) => unsafeObject[key] = this.escapeHtmlString(value));
        return unsafeObject;
    }

    /**
     * Creates dom element from the string
     * by parsing it as a documnet and returning the first child of the body.
     * @param {*} string 
     * @returns 
     */
    createElementFromString(string) {
        const document = new DOMParser().parseFromString(string, "text/html");
        return document.body.firstElementChild
    }

    /**
     * Removes all child nodes from the container.
     */
    clearChildren(container) {
        while (container.firstChild) {
            container.removeChild(container.lastChild);
        }
    }

    /**
     * Sets the appropriate display style for the element
     */
    showOrHideElement(elementId, condition) {
        document.getElementById(elementId).style.display = condition ? "flex" : "none";
    }
}

export default new htmlHelper();