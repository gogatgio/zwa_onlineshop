class formHelper {
    /**
     * Returns string representing the zwaInput div.
     * Contains input and error message div.
     * @param {*} id 
     * @param {*} placeholder 
     * @param {*} type 
     * @returns string
     */
    createZwaInput(id, placeholder, type = "text", defaultValue = "") {
        const attributeValue = defaultValue ? ` value="${defaultValue}" ` : "";
        const attributeType = type ? ` type="${type}"` : "";
        return `
            <div class="zwaInput">
                <input id="${id}" placeholder="${placeholder}" ${attributeType} ${attributeValue}>
                <div id="${this.getErrorMessageId(id)}" class="zwaInputErrorMessage"></div>
            </div>
        `;
    }

    /**
     * Generates the id for inpur error message div.
     * @param {*} inputId 
     * @returns string
     */
    getErrorMessageId(inputId) {
        return `${inputId}_ErrorMessage`;
    }
}

export default new formHelper();