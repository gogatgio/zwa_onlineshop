/**
 * Currency codes which are used during making an order
 */
const currencyCodes = Object.freeze({
    czk: "CZK",
    eur: "EUR"
});

export default currencyCodes;