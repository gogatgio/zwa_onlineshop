class cookieHelper {
    /**
     * Saves a cookie.
     * expirationDate parameter has a priority over expirationDays parameter
     * @param {*} cookieName 
     * @param {*} cookieValue 
     * @param {*} expirationDate default null
     * @param {*} expirationDays default 999 days
     */
    async setCookie(cookieName, cookieValue, expirationDate = null, expirationDays = 999) {
        let expires;

        if (expirationDate) {
            expires = "expires=" + expirationDate.toUTCString();
        } else {
            const date = new Date();
            date.setTime(date.getTime() + (expirationDays * 24 * 60 * 60 * 1000));
            expires = "expires=" + date.toUTCString();
        }

        document.cookie = `${cookieName}=${cookieValue}; ${expires}; path=/;`;
    }

    /**
     * Returns a cookie or if noen found then returns a default value.
     * @param {*} cookieName 
     * @param {*} defaultValue is an empty string
     * @returns 
     */
    async getCookieOrDefault(cookieName, defaultValue = "") {
        let name = cookieName + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let cookieArray = decodedCookie.split(';');
        for (let i = 0; i < cookieArray.length; i++) {
            let cookie = cookieArray[i];
            while (cookie.charAt(0) == ' ') {
                cookie = cookie.substring(1);
            }
            if (cookie.indexOf(name) == 0) {
                return cookie.substring(name.length, cookie.length);
            }
        }
        return defaultValue;
    }

    async deleteCookie(cookieName) {
        document.cookie = `${cookieName}=; max-age=-1; path=/;`;
    }
}

export default new cookieHelper();