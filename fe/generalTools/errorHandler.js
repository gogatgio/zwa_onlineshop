import logger from "./logger.js";
import logLevels from "./logLevels.js";

class errorHandler {
    /**
     * Logs the error comming from the BE
     */
    handleRequestError(error) {
        const errorText = JSON.stringify(error, null, 2);
        logger.log(errorText, logLevels.error);
    }

    /**
     * Returns a function that logs and shows the timeout message.
     * @param {*} message to be logged shown to the users
     * @returns a functon
     */
    getRequestTimeoutHandler(message) {
        return (progressEvent) => this.handleError(null, message, true);
    }

    /**
     * Logs and shows the error. At least one parameter has to be specified.
     * @param {*} error thrown in try catch blocks
     * @param {*} customMessage string to be shown to user. If not specified, then error message is used instead.
     * @param {*} show boolean specifying whether the error is to be shown to the user. Default false
     * @returns 
     */
    async handleError(error, customMessage = null, show = false) {
        // Check whether at least one parameter is given
        if (!error?.message && !customMessage) {
            return;
        }

        // Log
        const message = customMessage || error?.message;
        logger.log(message, logLevels.error, error?.stack ?? null);

        // Show
        if (show) {
            this.showError(message);
        }
    }

    /**
     * Shows the message to user.
     * @param {*} message string to be shown to user
     */
    showError(message) {
        alert(message);
    }
}

export default new errorHandler();