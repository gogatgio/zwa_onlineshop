import * as constants from "../constants.js";
import errorHandler from "./errorHandler.js";

class modalDialogHelper {

    /**
     * Opens the modal dialog and sets its header and content.
     * @param {*} headerText string that will be set as modalHeaderText.innerText
     * @param {*} content single dom element that will be set as a child of the modalContent
     * @param {*} closeByOuterClick determines whether the modalDialog should be closed when the user clicks outside of the modalDialog
     */
    async openModalDialog(headerText, content = null, closeByOuterClick = false) {
        try {
            this.modalDialog = this.getModalDialog();

            const modalHeaderText = this.modalDialog.querySelector(`#${constants.modalHeaderTextId}`);
            modalHeaderText.innerText = headerText;

            this.clearModalContent();
            if (content) {
                const modalContent = this.modalDialog.querySelector(`#${constants.modalContentId}`);
                modalContent.append(content);
            }

            this.modalDialog.style.display = "block";
            window.onclick = (event) => this.handlModalDialogClick.bind(this)(event, closeByOuterClick);
            
            // Focus first input in modal dialog if there are any
            const firstInput = content?.querySelector("input");
            if (firstInput) {
                firstInput.focus();
            }
        } catch (error) {
            errorHandler.handleError(error, this.errorMessage, true);
        }
    }

    /**
     * Removes all child nodes from the element.
     */
    clearModalContent() {
        const modalContent = this.modalDialog.querySelector(`#${constants.modalContentId}`);
        while (modalContent.firstChild) {
            modalContent.removeChild(modalContent.lastChild);
        }
    }

    /**
     * When the user clicks anywhere outside of the modalDialog, close it
     * @param {*} event 
     */
    handlModalDialogClick(event, closeByOuterClick) {
        if (closeByOuterClick && event.target == this.modalDialog) {
            this.closeModalDialog();
        }
    }

    /**
     * Sets modalDIalog style to none.
     */
    closeModalDialog() {
        try {
            this.modalDialog.style.display = "none";
            this.clearModalContent();
        } catch (error) {
            errorHandler.handleError(error, "Error closing the modal dialog.", true);
        }
    }

    /**
     * @returns modalDIalog dom element
     */
    getModalDialog() {
        return document.getElementById(constants.modalDialogId);
    }
}

export default new modalDialogHelper();