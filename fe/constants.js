import environment from "./config.js";

export const host = environment === "local" ? "http://localhost:8000" : "https://wa.toad.cz/~gogatgio";
export const callsUrl = `${host}/be/calls`;
export const defaultRequestTimeout = 5000;

export const getProductListURL = `${callsUrl}/getProductList.php`;
export const getProductURL = `${callsUrl}/getProduct.php`;
export const addProductURL = `${callsUrl}/addProduct.php`;
export const orderProductURL = `${callsUrl}/orderProduct.php`;
export const getOrderListURL = `${callsUrl}/getOrderList.php`;
export const deleteOrderURL = `${callsUrl}/deleteOrder.php`;
export const getAboutURL = `${callsUrl}/getAbout.php`;
export const logURL = `${callsUrl}/log.php`;
export const loginURL = `${callsUrl}/login.php`;
export const logoutURL = `${callsUrl}/logout.php`;
export const registerURL = `${callsUrl}/register.php`;
export const checkLoginURL = `${callsUrl}/checkLogin.php`;
export const locationURL = "https://nominatim.openstreetmap.org/reverse";

export const loggedOutControlsId = "loggedOutControls";
export const loggedInControlsId = "loggedInControls";
export const accountEmailId = "accountEmail";
export const loginButtonId = "loginButton";
export const logoutButtonId = "logoutButton";
export const registerButtonId = "registerButton";
export const productsTabButtonId = "productsTabButton";
export const ordersTabButtonId = "ordersTabButton";
export const addProductButtonId = "addProductButton";
export const sortButtonId = "sortButton";
export const searchButtonId = "searchButton";
export const binSvgId = "binSvg";
export const binLidHandleId = "lidHandle";
export const binLidBodyId = "lidBody";
export const aboutButtonId = "aboutButton";
export const closeModalDialogButtonId = "closeModalDialogButton";
export const backgroundAudioId ="backgroundAudio";
export const audioButtonId ="audioButton";
export const audioButtonCrossId = "audioButtonCross";
export const offlineIndicatorId = "offlineIndicator";


export const productsTabId = "productsTab";
export const ordersTabId = "ordersTab";
export const searchFormId ="searchForm";
export const searchTextFieldId = "searchTextField";
export const modalDialogId = "modalDialog";
export const modalHeaderTextId = "modalHeaderText";
export const modalContentId = "modalContent";
export const tabLinkClass = "tabLink";
export const activeTabClass = "activeTab";
export const tabContentClass = "tabContent";
export const productTileClass = "productTile";
export const productTileContainerClass = "productTileContainer";
export const productDetailClass = "productDetail";
export const orderTileClass = "orderTile";
export const orderTileContainerClass = "orderTileContainer";
export const nothingFoundClass = "nothingFound";

export const themesContainerId = "themesContainer";
export const themes = {
    blueThemeId: "blueTheme",
    greenThemeId: "greenTheme",
    redThemeId: "redTheme"
}
export const activeThemeClass = "activeTheme";
export const themeButtonClass = "themeButton";
export const themeKey = "theme";

export const defaultTab = { productsTabButtonId, productsTabId};
export const defaultTheme = themes.blueThemeId;