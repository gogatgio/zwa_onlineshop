<?php
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require_once "../controllers/logController.php";
require_once "../generalTools/logData.php";
require_once "../generalTools/logLevels.php";
require_once "../generalTools/logOrigins.php";
require_once "../generalTools/responseStatusCodes.php";
require_once "zwaGeneralException.php";
require_once "zwaValidationException.php";
require_once "zwaErrorCodes.php";

/**
 * Logs, sets http response code and returns the response.
 */
function handleExceptionOrError($e) {
    logExceptionOrError($e);
    return getResponse($e);
}

/**
 * Creates log entry in DB.
 */
function logExceptionOrError($e) {
    $logData = new logData(
        (new DateTime())->setTimezone(new DateTimeZone("UTC"))->format(DateTime::ATOM),
        logOrigins::server,
        logLevels::getLogLevelName(logLevels::error),
        $e->getMessage(),
        $e->getTraceAsString()
    );

    $logController = new logController();
    $logController->beLog($logData);
}

/**
 * Prepares error response object for http call.
 * Sets the http response status code.
 */
function getResponse($e) {
    $response = new stdClass();

    // Set error message - all exceptions and errors should have a message.
    $response->error = new stdClass();
    $response->error->message = $e->getMessage();

    // Set response status code
    if ($e instanceof zwaGeneralException) {
        http_response_code($e->getResponseStatusCode());

        // Set response zwa error code
        $zwaErrorCode = $e->getZwaErrorCode();
        if (isset($zwaErrorCode)) {
            $response->error->zwaErrorCode = $zwaErrorCode;
        }
    } else {
        http_response_code(responseStatusCodes::internalServerError);
    }

    // Set validationResults
    if ($e instanceof zwaValidationException) {
        $response->error->validationResults = $e->getValidationResults();
    }

    return $response;
}