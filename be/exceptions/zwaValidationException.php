<?php
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require_once "zwaGeneralException.php";

/**
 * Extends zwaGeneralException by adding validationResults.
 */
class zwaValidationException extends zwaGeneralException {
    protected $validationResults;

    public function __construct($validationResults, $responseStatusCode, $message, $errorCode = null, $code = 0, Throwable $previous = null) {
        // make sure everything is assigned properly in the parent
        parent::__construct($responseStatusCode, $message, $errorCode, $code, $previous);
        // set validationResults
        $this->validationResults = $validationResults;
    }

    final public function getValidationResults() {
        return $this->validationResults;
    }
}