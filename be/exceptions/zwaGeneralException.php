<?php
/**
 * Extends Exception by adding responseStatusCode.
 * ResponseStatusCode is then used in exceptionOrErrorHandler to set http response header.
 */
class zwaGeneralException extends Exception {
    protected $responseStatusCode;
    protected $zwaErrorCode;

    // Redefine the exception so message isn't optional
    public function __construct($responseStatusCode, $message, $zwaErrorCode = null, $code = 0, Throwable $previous = null) {
        // make sure everything is assigned properly in the parent
        parent::__construct($message, $code, $previous);
        // set zwaGeneralException properties
        $this->responseStatusCode = $responseStatusCode;
        $this->zwaErrorCode = $zwaErrorCode;
    }

    final public function getResponseStatusCode() {
        return $this->responseStatusCode;
    }

    final public function getZwaErrorCode() {
        return $this->zwaErrorCode;
    }
}