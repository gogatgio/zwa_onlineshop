<?php
/**
 * Custom error codes.
 */
abstract class zwaErrorCodes {
    const tokenExpired = "TOKEN_EXPIRED";
    const tokenInvalid = "TOKEN_INVALID";
}