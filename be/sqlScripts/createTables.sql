CREATE TABLE `gogatgio`.`products` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `description` VARCHAR(255) NULL,
  `price` DECIMAL(10) NOT NULL,
  `stock` INT NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `gogatgio`.`logs` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `utcdatetime` DATETIME NOT NULL,
  `origin` VARCHAR(20) NOT NULL,
  `level` VARCHAR(20) NOT NULL,
  `message` VARCHAR(255) NOT NULL,
  `stack` VARCHAR(5000),
  PRIMARY KEY (`id`));


CREATE TABLE `gogatgio`.`profiles` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(20) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC) VISIBLE);

CREATE TABLE `gogatgio`.`accounts` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(50) NOT NULL,
  `passwordHash` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE);

CREATE TABLE `gogatgio`.`account_profiles` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `accountId` INT NOT NULL,
  `profileId` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_idx` (`accountId` ASC) VISIBLE,
  INDEX `id_idx1` (`profileId` ASC) VISIBLE,
  CONSTRAINT `account_profiles_accountId`
    FOREIGN KEY (`accountId`)
    REFERENCES `gogatgio`.`accounts` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `account_profiles_profileId`
    FOREIGN KEY (`profileId`)
    REFERENCES `gogatgio`.`profiles` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE TABLE `gogatgio`.`login_sessions` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `accountId` INT NOT NULL,
  `utcExpirationDatetime` DATETIME NOT NULL,
  `jwtSecret` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `login_sessions_accountId`
    FOREIGN KEY (`accountId`)
    REFERENCES `gogatgio`.`accounts` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);

CREATE EVENT IF NOT EXISTS `login_sessions_cleaner_event`
    ON SCHEDULE
      EVERY 1 DAY
        STARTS '2022-01-06 00:00:00'
        ON COMPLETION PRESERVE ENABLE 
      DO
        DELETE FROM login_sessions
        WHERE utcExpirationDatetime <= UTC_TIMESTAMP();

CREATE TABLE `gogatgio`.`currencies` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(20) NOT NULL,
  `shortName` VARCHAR(45) NOT NULL,
  `name` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `code_UNIQUE` (`code` ASC) VISIBLE);

CREATE TABLE `gogatgio`.`orders` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `accountId` INT NOT NULL,
  `productId` INT NOT NULL,
  `productCount` INT NOT NULL,
  `orderPrice` DECIMAL(10,2) NOT NULL,
  `currencyId` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `id_idx` (`accountId` ASC) VISIBLE,
  INDEX `id_idx1` (`productId` ASC) VISIBLE,
  CONSTRAINT `orders_accountId`
    FOREIGN KEY (`accountId`)
    REFERENCES `gogatgio`.`accounts` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `orders_productId`
    FOREIGN KEY (`productId`)
    REFERENCES `gogatgio`.`products` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `orders_currencyId`
    FOREIGN KEY (`currencyId`)
    REFERENCES `gogatgio`.`currencies` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);