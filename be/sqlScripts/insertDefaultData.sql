INSERT INTO gogatgio.products
	(name, description, price, stock)
VALUES
	('Jablko', 'Poctivé červené české jablko.', 10, 1000),
	('Auto', 'Nová tesla. Modrá barva. Dlouhá záruka', 2000000, 2),
	('Krabice', 'Kartonová krabice z recyklovaného papíru. 1x1m', 5, 5000),
	('Mobil', 'Apple mobil. Nenovější model.', 8000, 20),
	('Sešit', 'Poctivé červené české jablko.', 5, 1000),
	('Žárovka', 'LED žárovka. 60mm. 45W', 300, 500),
    ('Husa', 'Čerstvá husa', 80, 400),
    ('Kachna', 'Čerstvá česká kachna', 90, 300),
    ('Počítač', 'Nový a barevný', 15000, 50),
    ('Notebook', 'Malý a lehký', 16000, 50),
    ('Šroubovák', 'do kříže', 79, 100),
    ('Bonbóny', 'Jáhodové. Balení po 100.', 100, 600),
    ('Protéza', 'Protáza na ruku', 40000, 3),
    ('Papoušek', 'Červeno-modrý. 1 rok starý.', 2000, 1),
    ('Peněženka', 'Kožená', 499, 30),
    ('Taška', 'Igelitová', 2, 5000),
    ('Záclony', 'Pár záclon. 3m dlouhé.', 1999, 70),
    ('Nabíječka', '5V/3A 15W', 199, 500),
    ('Paruka', 'Černé kudrnaté vlasy', 999, 10),
    ('Dveře', 'Dubové', 2999, 15),
    ('Propiska', 'Modrý vyměnitelný inkoust', 59, 1000),
    ('Kytka', 'Růže', 199, 200),
    ('Písek', '5kg balení', 59, 300),
    ('Kravata', 'Modro-bílá kostkovaná', 599, 100),
    ('Krevety', 'Balení po 10ks', 299, 450),
    ('Štětec', 'Plochý široký 3cm.', 99, 1500),
    ('Obraz', 'Náhodné překvápko', 999, 200),
    ('Sluchátka', 'Špunty do uší', 299, 200),
    ('Strom', 'Jabloň 1 rok staará', 4999, 10),
    ('Pes', 'Štěné 2 měsíce staré', 2999, 5),
    ('Ubrousky', 'Balení po 50ks.', 100, 2000),
    ('Pásek', 'Kožený pásek 3m dlouhý.', 599, 100),
    ('Větrák', 'Středně velký.', 1999, 30),
    ('Tarantule', 'Vycpaná tarantule', 799, 40),
    ('Odšťavnovač', 'Velký odšťavnovač. 50W', 4999, 10),
    ('Plyšák', 'opice / slon / pes', 499, 2000),
    ('Stolní hra', 'Pro 2-5 hráčů.', 999, 50),
    ('Dron', 'Výdrž 2h. HD kamera.', 9999, 20),
    ('Lupa', 'Lupa pro děti.', 899, 10);

INSERT INTO gogatgio.profiles
	(code, name, description)
VALUES
	('BASIC', 'Základní', 'Profil běžného uživatele.'),
	('MANAGER', 'Manažerský', 'Profil manažera s právy pro správu produktů a správu veškerých objednávek.');

INSERT INTO gogatgio.currencies
	(code, shortName, name)
VALUES
	('CZK', 'Kč', 'Koruna česká'),
	('EUR', 'Euro', 'Euro');