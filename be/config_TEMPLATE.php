<?php
    define('ENVIRONMENT', 'local'); // toad, local

    define('TOAD_USER', ''); // required only for toad
    define('TOAD_PASSWORD', ''); // required only for toad

    define('DB_HOST', '');
    define('DB_NAME', '');
    define('DB_USERNAME', '');
    define('DB_PASSWORD', '');
?>