<?php
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require_once "baseDtoInTypes.php";
require_once "baseValueTypes.php";

class GetOrderListDtoInType extends zwaBaseDtoInType {
    public $sort;
    public $pageIndex;
    public $pageSize;

    public function __construct() {
        $this->sort = new zwaString(null, 10);
        $this->pageIndex = new zwaInt(0);
        $this->pageSize = new zwaInt(1);
    }
}

class DeleteOrderDtoInType extends zwaBaseDtoInType {
    public $id;

    public function __construct() {
        $this->id = new zwaInt(0, null, true);
    }
}
