<?php
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require_once "baseDtoInTypes.php";
require_once "baseValueTypes.php";
require_once "../generalTools/currencyCodes.php";

class getProductListDtoInType extends zwaBaseDtoInType {
    public $sort;
    public $search;
    public $pageIndex;
    public $pageSize;

    public function __construct() {
        $this->sort = new zwaString(null, 10);
        $this->search = new zwaString(null, 100);
        $this->pageIndex = new zwaInt(0);
        $this->pageSize = new zwaInt(1);
    }
}

class getProductDtoInType extends zwaBaseDtoInType {
    public $id;

    public function __construct() {
        $this->id = new zwaInt(0, null, true);
    }
}

class orderProductDtoInType extends zwaBaseDtoInType {
    public $orderProductId;
    public $orderProductCount;
    public $orderProductCurrency;

    public function __construct() {
        $this->orderProductId = new zwaInt(0, null, true);
        $this->orderProductCount = new zwaInt(0, null, true);
        $this->orderProductCurrency = new zwaEnum([currencyCodes::CZK, currencyCodes::EUR], true);
    }
}

class addProductDtoInType extends zwaBaseDtoInType {
    public $addProductName;
    public $addProductDescription;
    public $addProductPrice;
    public $addProductStock;

    public function __construct() {
        $this->addProductName = new zwaString(null, 100, true);
        $this->addProductDescription = new zwaString(null, 255);
        $this->addProductPrice = new zwaInt(0, null, true);
        $this->addProductStock = new zwaInt(0, null, true);
    }
}
