<?php
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require_once "baseDtoInTypes.php";
require_once "baseValueTypes.php";

class logDtoInType extends zwaBaseDtoInType {
    public $utcdatetime;
    public $origin;
    public $level;
    public $message;
    public $stack;

    public function __construct() {
        $this->utcdatetime = new zwaDateTime(true);
        $this->origin = new zwaString(null, 20, true);
        $this->level = new zwaString(null, 20, true);
        $this->message = new zwaString(null, 255, true);
        $this->stack = new zwaString(null, 5000);
    }
}


