<?php
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require_once "baseDtoInTypes.php";
require_once "baseValueTypes.php";
require_once "../generalTools/responseStatusCodes.php";
require_once "../exceptions/zwaValidationException.php";

class Validator {
    public function validate($dtoIn, zwaBaseDtoInType $dtoInType) {
        $validationResults = array();

        foreach ($dtoInType as $key => $valueType) {
            // make sure we are checking zwaBaseValueType
            if (!isset($valueType) || !($valueType instanceof zwaBaseValueType)) {
                continue;
            }

            // prepare a single validationResult object 
            $validationResult = new stdClass();
            $validationResult->key = $key;
            $validationResult->errors = array();

            $value = $dtoIn->$key;

            // check specific value types
            $errors = $valueType->validate($value);
            if (count($errors) > 0) {
                $validationResult->errors = array_merge($validationResult->errors, $errors);
            }

            // push validationResult into validationResults if any
            if (count($validationResult->errors) > 0) {
                array_push($validationResults, $validationResult);
            }
        }

        // call customValidation
        if (isset($dtoInType->customValidation)) {
            $validationResult = call_user_func($dtoInType->customValidation, $dtoIn);

            if (isset($validationResult)) {
                array_push($validationResults, $validationResult);
            }
        }

        // If there are any errors throw a zwaValidationException
        if (count($validationResults) > 0) {
            $this->throwZwaValidationException($validationResults);
        }
    }

    public function throwZwaValidationException($validationResults) {
        throw new zwaValidationException($validationResults, responseStatusCodes::badRequest, "Validation errors.");
    }
}
