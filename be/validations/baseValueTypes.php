<?php
abstract class zwaBaseValueType {
    public $required;

    public function __construct($required = false)
    {
        $this->required = $required;
    }

    /**
     * Checks if the required value is set.
     */
    public function validate($value) {
        $errors = array();

        if ($this->required && !isset($value)) {
            array_push($errors, "Value is required");
        }

        return $errors;
    }

    /**
     * If the value is not required and is not set,
     * then we don't need to validate anything.
     */
    public function validationRequired($value) {
        if (!$this->required && !isset($value)) {
            return false;
        }

        return true;
    }
}

class zwaString extends zwaBaseValueType {
    public $maxLength;
    public $minLength;

    public function __construct($minLength = null, $maxLength = null, $required = false)
    {
        parent::__construct($required);
        $this->maxLength = $maxLength;
        $this->minLength = $minLength;
    }

    public function validate($value) {
        $errors = array();

        // Check if validation needed
        if (!$this->validationRequired($value)) {
            return $errors;
        }

        // Check required
        // If it's required and not set, then we just return this error.
        // Without a value there is nothing else to check.
        $parentErrors = parent::validate($value);
        if (count($parentErrors) > 0) {
            return $parentErrors;
        }

        // Check string
        if (!is_string($value)) {
            array_push($errors, "Value $value is not a string.");
        }

        // Check min length
        if (isset($this->minLength) && strlen($value) < $this->minLength) {
            array_push($errors, "Value $value is shorter that the min allowed length $this->minLength.");
        }

        // Check max length
        if (isset($this->maxLength) && strlen($value) > $this->maxLength) {
            array_push($errors, "Value $value exceeds the max allowed length $this->maxLength.");
        }

        return $errors;
    }
}

class zwaEmail extends zwaString {
    public function __construct($required = false)
    {
        parent::__construct(0, 50, $required);
    }

    public function validate($value) {
        $errors = array();

        // Check if validation needed
        if (!$this->validationRequired($value)) {
            return $errors;
        }

        // Check parent validations - minLength, maxLength, required
        $parentErrors = parent::validate($value);
        if (count($parentErrors) > 0) {
            return $parentErrors;
        }

        // Check email format
        $emailFormat = "/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/";
        if (!preg_match($emailFormat, $value)) {
            array_push($errors, "Email $value does not a have a correct format.");
        }

        return $errors;
    }
}

class zwaPassword extends zwaString {
    public function __construct()
    {
        parent::__construct(6, 50, true);
    }

    public function validate($value) {
        $errors = array();

        // Check if validation needed
        if (!$this->validationRequired($value)) {
            return $errors;
        }

        // Check parent validations - minLength, maxLength, required
        $parentErrors = parent::validate($value);
        if (count($parentErrors) > 0) {
            return $parentErrors;
        }

        // Check number
        if (!preg_match("@[0-9]@", $value)) {
            array_push($errors, "Password must contain a number.");
        }

        // Check uppercase
        if (!preg_match("@[a-z]@", $value)) {
            array_push($errors, "Password must contain a lowercase character.");
        }

        // Check lowercase
        if (!preg_match("@[A-Z]@", $value)) {
            array_push($errors, "Password must contain an uppercase character.");
        }

        return $errors;
    }
}

class zwaInt extends zwaBaseValueType {
    public $min;
    public $max;

    public function __construct($min = null, $max = null, $required = false)
    {
        parent::__construct($required);
        $this->min = $min;
        $this->max = $max;
    }

    public function validate($value) {
        $errors = array();

        // Check if validation needed
        if (!$this->validationRequired($value)) {
            return $errors;
        }

        // Check required
        $parentErrors = parent::validate($value);
        if (count($parentErrors) > 0) {
            return $parentErrors;
        }

        // Check int
        if (!is_int($value)) {
            array_push($errors, "Value $value is not an inteager.");
        }

        // Check min
        if (isset($this->min) && $value < $this->min) {
            array_push($errors, "Value $value is less than min allowed value $this->min");
        }

        // Check max
        if (isset($this->max) && $value > $this->max) {
            array_push($errors, "Value $value is greater than max allowed value $this->max");
        }

        return $errors;
    }
}

class zwaDateTime extends zwaBaseValueType {
    public function validate($value) {
        $errors = array();

        // Check if validation needed
        if (!$this->validationRequired($value)) {
            return $errors;
        }

        // Check required
        $parentErrors = parent::validate($value);
        if (count($parentErrors) > 0) {
            return $parentErrors;
        }

        // Check dateTime
        try {
            $parsedDateTime = new DateTime($value);
        } catch (Exception $ex) {
            array_push($errors, "Value $value is not a correct iso date time format.");
        }

        return $errors;
    }
}

class zwaEnum extends zwaBaseValueType {
    public $values;

    public function __construct($values, $required = false)
    {
        parent::__construct($required);
        $this->values = $values;
    }

    public function validate($value) {
        $errors = array();

        // Check if validation needed
        if (!$this->validationRequired($value)) {
            return $errors;
        }

        // Check required
        $parentErrors = parent::validate($value);
        if (count($parentErrors) > 0) {
            return $parentErrors;
        }

        // Check enum value
        if (!in_array($value, $this->values)) {
            array_push($errors, "Value $value is not a valid enum value.");
        }

        return $errors;
    }
}