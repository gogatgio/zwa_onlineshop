<?php
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require_once "baseDtoInTypes.php";
require_once "baseValueTypes.php";

class loginDtoInType extends zwaBaseDtoInType {
    public $loginEmail;
    public $loginPassword;

    public function __construct() {
        $this->loginEmail = new zwaEmail(true);
        $this->loginPassword = new zwaPassword();
    }
}

class registerDtoInType extends zwaBaseDtoInType {
    public $registerEmail;
    public $registerPassword1;
    public $registerPassword2;

    public function __construct() {
        $this->registerEmail = new zwaEmail(true);
        $this->registerPassword1 = new zwaPassword();
        $this->registerPassword2 = new zwaPassword();

        $passwordsEqualValidation = function($dtoIn) {
            $validationResult = new stdClass();
            $validationResult->key = "registerPassword2";
            $validationResult->errors = array();
            if ($dtoIn->registerPassword1 !== $dtoIn->registerPassword2) {
                array_push($validationResult->errors, "Password must be same.");
                return $validationResult;
            }
        };
        $this->customValidation = $passwordsEqualValidation;
    }
}
