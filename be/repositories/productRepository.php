<?php
require_once "baseRepository.php";
require_once "../generalTools/pageInfo.php";
require_once "../generalTools/sortOptions.php";

class ProductRepository extends baseRepository {
    /**
     * Returns an array of objects.
     */
    function getProductList($getProductListDtoIn) {
        $select = "id, name";
        $offset = $getProductListDtoIn->pageIndex * $getProductListDtoIn->pageSize;
        $limit = "LIMIT $offset, $getProductListDtoIn->pageSize";
        return $this->selectProducts($getProductListDtoIn->sort, $getProductListDtoIn->search, $select, $limit);
    }

    /**
     * Returns an object pageInfo.
     */
    function getPageInfo($getProductListDtoIn) {
        $select = "COUNT(*) as productCount";
        $limit = "";
        $productCount = $this->selectProducts($getProductListDtoIn->sort, $getProductListDtoIn->search, $select, $limit)[0]->productCount;
        $pageCount = ceil($productCount / $getProductListDtoIn->pageSize);
        return new pageInfo($getProductListDtoIn->pageIndex, $getProductListDtoIn->pageSize, $pageCount);
    }

    private function selectProducts($sort, $search, $select, $limit) {
        $conn = $this->getDbConnection();

        $conditions = array();
        if (isset($search)) {
            array_push($conditions, "name LIKE :search");
        }

        $where = "";
        if (count($conditions) > 0) {
            $joinedConditions = join(" AND ", $conditions);
            $where = "WHERE $joinedConditions";
        }

        $orderBy = "ORDER BY ".((isset($sort) && strtoupper($sort) === strtoupper(sortOptions::new)) ? "id DESC" : "name");

        $query = "
            SELECT $select
            FROM products
            $where
            $orderBy
            $limit
        ";

        $stmt = $conn->prepare($query);

        if (isset($search)) {
            $search = "%{$search}%"; // searched value can be anywhere in the name
            $stmt->bindParam(":search", $search);
        }

        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_OBJ);

        $productList = $stmt->fetchAll();

        return $productList;
    }

    function getProduct($id) {
        $conn = $this->getDbConnection();

        $query = "
            SELECT *
            FROM products
            WHERE id = :id;
        ";

        $stmt = $conn->prepare($query);

        $stmt->bindParam(":id", $id, PDO::PARAM_INT);

        $stmt->execute();

        $product = $stmt->fetch(PDO::FETCH_OBJ);

        return $product;
    }

    /**
     * @return integer productId
     */
    function addProduct($addProductData) {
        $conn = $this->getDbConnection();

        $query = "
            INSERT INTO products
                (name, description, price, stock)
            VALUES
                (:name, :description, :price, :stock);
        ";

        $stmt = $conn->prepare($query);

        $stmt->bindParam(":name", $addProductData->addProductName);
        $stmt->bindParam(":description", $addProductData->addProductDescription);
        $stmt->bindParam(":price", $addProductData->addProductPrice, PDO::PARAM_INT);
        $stmt->bindParam(":stock", $addProductData->addProductStock, PDO::PARAM_INT);

        $stmt->execute();

        $productId = (int)$conn->lastInsertId();

        return $productId;
    }

    /**
     * @return integer productId
     */
    function updateProductStock($id, $stockDelta) {
        $conn = $this->getDbConnection();

        $query = "
            UPDATE products
            SET stock = stock + :stockDelta
            WHERE id = :id;
        ";

        $stmt = $conn->prepare($query);

        $stmt->bindParam(":id", $id, PDO::PARAM_INT);
        $stmt->bindParam(":stockDelta", $stockDelta, PDO::PARAM_INT);

        $stmt->execute();

        return $id;
    }
}
