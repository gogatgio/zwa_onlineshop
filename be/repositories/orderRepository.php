<?php
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require_once "baseRepository.php";
require_once "../generalTools/pageInfo.php";
require_once "../generalTools/sortOptions.php";


class OrderRepository extends baseRepository {
    /**
     * @return integer orderId
     */
    function createOrder($accountId, $productId, $productCount, $orderPrice, $currencyId) {
        $conn = $this->getDbConnection();

        $query = "
            INSERT INTO orders
                (accountId, productId, productCount, orderPrice, currencyId)
            VALUES
                (:accountId, :productId, :productCount, :orderPrice, :currencyId);
        ";

        $stmt = $conn->prepare($query);

        $stmt->bindParam(":accountId", $accountId, PDO::PARAM_INT);
        $stmt->bindParam(":productId", $productId, PDO::PARAM_INT);
        $stmt->bindParam(":productCount", $productCount, PDO::PARAM_INT);
        $stmt->bindParam(":orderPrice", $orderPrice, PDO::PARAM_STR);
        $stmt->bindParam(":currencyId", $currencyId, PDO::PARAM_INT);

        $stmt->execute();

        $orderId = (int)$conn->lastInsertId();

        return $orderId;
    }

    function getOrder($accountId, $orderId) {
        $conn = $this->getDbConnection();

        $query = "
            SELECT *
            FROM orders
            WHERE accountId = :accountId
            AND id = :orderId;
        ";

        $stmt = $conn->prepare($query);

        $stmt->bindParam(":accountId", $accountId, PDO::PARAM_INT);
        $stmt->bindParam(":orderId", $orderId, PDO::PARAM_INT);

        $stmt->execute();

        $order = $stmt->fetch(PDO::FETCH_OBJ);

        return $order;
    }

    /**
     * Returns an array of objects.
     */
    function getOrderList($accountId, $getOrderListDtoIn) {
        $select = "orders.id id, products.name name, orders.productCount productCount, orders.orderPrice orderPrice, currencies.shortName currency";
        $offset = $getOrderListDtoIn->pageIndex * $getOrderListDtoIn->pageSize;
        $limit = "LIMIT $offset, $getOrderListDtoIn->pageSize";
        return $this->selectOrders($accountId, $getOrderListDtoIn->sort, $select, $limit);
    }

    /**
     * Returns an object pageInfo.
     */
    function getPageInfo($accountId, $getOrderListDtoIn) {
        $select = "COUNT(*) as orderCount";
        $limit = "";
        $orderCount = $this->selectOrders($accountId, $getOrderListDtoIn->sort, $select, $limit)[0]->orderCount;
        $pageCount = ceil($orderCount / $getOrderListDtoIn->pageSize);
        return new pageInfo($getOrderListDtoIn->pageIndex, $getOrderListDtoIn->pageSize, $pageCount);
    }

    private function selectOrders($accountId, $sort, $select, $limit) {
        $conn = $this->getDbConnection();

        $conditions = array();
        array_push($conditions, "orders.accountId = :accountId");

        $where = "";
        if (count($conditions) > 0) {
            $joinedConditions = join(" AND ", $conditions);
            $where = "WHERE $joinedConditions";
        }

        $orderBy = "ORDER BY ".((isset($sort) && strtoupper($sort) === strtoupper(sortOptions::new)) ? "orders.id DESC" : "name");

        $query = "
            SELECT $select
            FROM orders
            inner join products on products.id = orders.productId
            inner join currencies on currencies.id = orders.currencyId
            $where
            $orderBy
            $limit
        ";

        $stmt = $conn->prepare($query);

        $stmt->bindParam(":accountId", $accountId);

        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_OBJ);

        $productList = $stmt->fetchAll();

        return $productList;
    }

    function deleteOrder($accountId, $orderId) {
        $conn = $this->getDbConnection();

        $query = "
            DELETE
            FROM orders
            WHERE accountId = :accountId
            AND id = :orderId;
        ";

        $stmt = $conn->prepare($query);

        $stmt->bindParam(":accountId", $accountId, PDO::PARAM_INT);
        $stmt->bindParam(":orderId", $orderId, PDO::PARAM_INT);

        $stmt->execute();

        $rowsAffected = $stmt->rowCount();

        return $rowsAffected;
    }
}
