<?php
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require_once "baseRepository.php";
require_once "../generalTools/logData.php";


class LogRepository extends baseRepository {
    /**
     * @return integer logId
     */
    function log(logData $logData) {
        $conn = $this->getDbConnection();

        $query = "
            INSERT INTO logs (utcdatetime, origin, level, message, stack)
            VALUES (:utcdatetime, :origin, :level, :message, :stack);
        ";

        $stmt = $conn->prepare($query);

        $formatedUtcDateTime = $this->formatIsoStringDateTime($logData->utcdatetime);
        $stmt->bindParam(":utcdatetime", $formatedUtcDateTime);
        $stmt->bindParam(":origin", $logData->origin);
        $stmt->bindParam(":level", $logData->level);
        $stmt->bindParam(":message", $logData->message);
        $stmt->bindParam(":stack", $logData->stack);

        $stmt->execute();

        $logId = (int)$conn->lastInsertId();

        return $logId;
    }
}
