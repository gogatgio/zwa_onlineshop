<?php
require_once "../config.php";

abstract class baseRepository {
    /**
     * Returns a new PDO DB connection.
     */
    protected function getDbConnection() {
        $conn = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USERNAME, DB_PASSWORD);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
    }

    /**
     * Formats isoStringDateTime to a format accepted in MySQL SB.
     * Example: 2021-12-26T12:51:18.904Z -> 2021-12-26 12:51:18
     */
    protected function formatIsoStringDateTime($isoStringDateTime) {
        $dateTime = new DateTime($isoStringDateTime);
        return $this->formatDateTime($dateTime);
    }

    /**
     * Formats DateTime to a fromat accepted in MySQL SB.
     */
    protected function formatDateTime($dateTime) {
        return $dateTime->format("Y-m-d H:i:s");
    }
}
