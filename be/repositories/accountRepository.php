<?php
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require_once "baseRepository.php";
require_once "../controllers/dtos/loginDtoIn.php";
require_once "../controllers/dtos/registerDtoIn.php";


class AccountRepository extends baseRepository {
    /**
     * @return boolean duplicit
     */
    function checkEmailDuplicity($email) {
        $conn = $this->getDbConnection();

        $query = "
            SELECT EXISTS (
                SELECT 1
                FROM accounts
                WHERE email = :email
            ) as duplicit;
        ";

        $stmt = $conn->prepare($query);

        $stmt->bindParam(":email", $email);

        $stmt->execute();

        $duplicit = $stmt->fetch(PDO::FETCH_NUM)[0];

        return boolval($duplicit);
    }

    /**
     * @return integer accountId
     */
    function createAccount($email, $passwordHash) {
        $conn = $this->getDbConnection();

        $query = "
            INSERT INTO accounts
                (email, passwordHash)
            VALUES
                (:email, :passwordHash);
        ";

        $stmt = $conn->prepare($query);

        $stmt->bindParam(":email", $email);
        $stmt->bindParam(":passwordHash", $passwordHash);

        $stmt->execute();

        $accountId = (int)$conn->lastInsertId();

        return $accountId;
    }

    /**
     * @return object account
     */
    function getAccount($email) {
        $conn = $this->getDbConnection();

        $query = "
            SELECT *
            FROM accounts
            WHERE email = :email;
        ";

        $stmt = $conn->prepare($query);

        $stmt->bindParam(":email", $email);

        $stmt->execute();

        $account = $stmt->fetch(PDO::FETCH_OBJ);

        return $account;
    }

    /**
     * @return object profile
     */
    function getProfile($code) {
        $conn = $this->getDbConnection();

        $query = "
            SELECT *
            FROM profiles
            WHERE code = :code;
        ";

        $stmt = $conn->prepare($query);

        $stmt->bindParam(":code", $code);

        $stmt->execute();

        $profile = $stmt->fetch(PDO::FETCH_OBJ);

        return $profile;
    }

    /**
     * @return integer accountProfileId
     */
    function createAccountProfile($accountId, $profileId) {
        $conn = $this->getDbConnection();

        $query = "
            INSERT INTO account_profiles
                (accountId, profileId)
            VALUES
                (:accountId, :profileId);
        ";

        $stmt = $conn->prepare($query);

        $stmt->bindParam(":accountId", $accountId, PDO::PARAM_INT);
        $stmt->bindParam(":profileId", $profileId, PDO::PARAM_INT);

        $stmt->execute();

        $accountProfileId = (int)$conn->lastInsertId();

        return $accountProfileId;
    }

    /**
     * Returns array of profile codes.
     * @return array of strings
     */
    function getAccountProfileList($accountId) {
        $conn = $this->getDbConnection();

        $query = "
            SELECT profiles.code
            FROM account_profiles
            JOIN profiles ON profiles.id = account_profiles.profileId
            WHERE accountId = :accountId;
        ";

        $stmt = $conn->prepare($query);

        $stmt->bindParam(":accountId", $accountId, PDO::PARAM_INT);

        $stmt->execute();

        $accountProfileList = $stmt->fetchAll(PDO::FETCH_OBJ);

        function getCode(&$value, $key)
        {
            $value=$value->code;
        }

        array_walk($accountProfileList, "getCode");

        return $accountProfileList;
    }

    /**
     * @return integer loginSessionId
     */
    function createLoginSession($accountId, $utcExpirationDatetime, $jwtSecret) {
        $conn = $this->getDbConnection();

        $query = "
            INSERT INTO login_sessions
                (accountId, utcExpirationDatetime, jwtSecret)
            VALUES
                (:accountId, :utcExpirationDatetime, :jwtSecret);
        ";

        $stmt = $conn->prepare($query);

        $formatDateTime = $this->formatDateTime($utcExpirationDatetime);
        $stmt->bindParam(":accountId", $accountId, PDO::PARAM_INT);
        $stmt->bindParam(":utcExpirationDatetime", $formatDateTime);
        $stmt->bindParam(":jwtSecret", $jwtSecret);

        $stmt->execute();

        $loginSessionId = (int)$conn->lastInsertId();

        return $loginSessionId;
    }

    /**
     * Returns only login sessions that are not expired.
     * @return obejct loginSession
     */
    function getLoginSession($sessionId) {
        $conn = $this->getDbConnection();

        $query = "
            SELECT *
            FROM login_sessions
            WHERE id = :sessionId
            AND utcExpirationDatetime > UTC_TIMESTAMP();
        ";

        $stmt = $conn->prepare($query);

        $stmt->bindParam(":sessionId", $sessionId, PDO::PARAM_INT);

        $stmt->execute();

        $loginSession = $stmt->fetch(PDO::FETCH_OBJ);

        return $loginSession;
    }

    /**
     * Deletes the loginSession.
     * @return nothing
     */
    function deleteLoginSession($sessionId) {
        $conn = $this->getDbConnection();

        $query = "
            DELETE FROM login_sessions
            WHERE id = :sessionId;
        ";

        $stmt = $conn->prepare($query);

        $stmt->bindParam(":sessionId", $sessionId, PDO::PARAM_INT);

        $stmt->execute();
    }
}
