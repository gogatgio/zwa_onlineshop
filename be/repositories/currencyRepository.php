<?php
require_once "baseRepository.php";

class CurrencyRepository extends baseRepository {
    /**
     * @return object currency
     */
    function getCurrency($code) {
        $conn = $this->getDbConnection();

        $query = "
            SELECT *
            FROM currencies
            WHERE code = :code;
        ";

        $stmt = $conn->prepare($query);

        $stmt->bindParam(":code", $code);

        $stmt->execute();

        $currency = $stmt->fetch(PDO::FETCH_OBJ);

        return $currency;
    }
}
