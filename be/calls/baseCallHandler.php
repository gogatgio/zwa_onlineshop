<?php
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require_once "../exceptions/exceptionOrErrorHandler.php";
require_once "../exceptions/zwaErrorCodes.php";
require_once "../exceptions/zwaGeneralException.php";
require_once "../generalTools/responseStatusCodes.php";
require_once "../generalTools/jwtHelper.php";
require_once "../generalTools/profileCodes.php";
require_once "../controllers/accountController.php";

$authentication;

/**
 * Checks the requestMethod a calls the appropriate controller function.
 * Takes care of the error handling as well.
 * @param boolean $respondWithResultJson if false, then an empty json is returned {}
 */
function handleCall($requestMethod, $allowedProfiles, $controller, $function, $respondWithResultJson = true) {
    // Always return at least an empty json {}
    $response = "{}";

    // Set custom error handler to turn warnings into Exceptions
    set_error_handler(function ($severity, $message, $file, $line) {
        throw new zwaGeneralException(responseStatusCodes::internalServerError, $message);
    });

    try {
        // Check request method
        if ($_SERVER["REQUEST_METHOD"] === $requestMethod) {
            // Check authorization
            checkAuthorization($allowedProfiles);
            // Call the controller function
            $result =  call_user_func(array($controller, $function));
        } else {
            throw new zwaGeneralException(responseStatusCodes::methodNotAllowed, "$requestMethod request method not allowed for $function.");
        }

        // Prepare response json if necessary
        if ($respondWithResultJson) {
            $response = json_encode($result);
        }
    } catch (Exception | Error $e) {
        // Prepare response error json
        $response = json_encode(handleExceptionOrError($e));
    }

    // Restore the previous error handler
    restore_error_handler();

    // Return json to FE
    echo $response;
}

/**
 * Checks request bearer token and sets the global authentication variable
 */
function checkAuthorization($allowedProfiles) {
    global $authentication;

    $jwtHelper = new JwtHelper();
    
    $anonymousAllowed = in_array(profileCodes::ANONYMOUS, $allowedProfiles);

    // Get bearer token
    $jwt = $jwtHelper->getBearerToken();

    // Bearer not set
    if (!isset($jwt)) {
        if ($anonymousAllowed) {
            // ANONYMOUS allowed
            return;
        } else {
            // ANONYMOUS not allowed
            throw new zwaGeneralException(responseStatusCodes::unauthorized, "Authentication required. Please log in.");
        }
    }

    // Get JWT payload
    $jwtPayload = $jwtHelper->getJwtPayload($jwt);

    // Validate JWT - Expiration, signature
    $jwtExpirationValid = $jwtHelper->validateJwtExpiration($jwt);
    if (!$jwtExpirationValid) {
        throw new zwaGeneralException(responseStatusCodes::unauthorized, "Expired bearer token. Please log in again.", zwaErrorCodes::tokenExpired);
    }

    // Validate JWT - Signature
    $accountController = new AccountController();
    $jwtSecret = $accountController->getLoginSessionJwtSecret($jwtPayload->sessionId);
    if (!$jwtSecret) {
        throw new zwaGeneralException(responseStatusCodes::unauthorized, "Couldn't get jwt secret.", zwaErrorCodes::tokenInvalid);
    }
    $jwtValidSignature = $jwtHelper->validateJwtSignature($jwt, $jwtSecret);
    if (!$jwtValidSignature) {
        throw new zwaGeneralException(responseStatusCodes::unauthorized, "Invalid signature.", zwaErrorCodes::tokenInvalid);
    }

    // Check allowed profiles
    $jwtProfiles = $jwtPayload->profiles;
    $allowedJwtProfiles = array_intersect($allowedProfiles, $jwtProfiles);
    if (!$anonymousAllowed && count($allowedJwtProfiles) === 0) {
        throw new zwaGeneralException(responseStatusCodes::forbidden, "Unathorized. Please log in with an authorized account.");
    }

    // Set global authentication variable
    $authentication = $jwtPayload;
}
