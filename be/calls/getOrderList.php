<?php
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require_once "./baseCallHandler.php";
require_once "../controllers/orderController.php";

handleCall(
    "GET",
    [profileCodes::BASIC, profileCodes::MANAGER],
    new OrderController(),
    "getOrderList"
);