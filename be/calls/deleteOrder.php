<?php
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require_once "./baseCallHandler.php";
require_once "../controllers/orderController.php";

handleCall(
    // DELETE not allowed by apache server at toad
    "POST",
    [profileCodes::BASIC, profileCodes::MANAGER],
    new OrderController(),
    "deleteOrder",
    false
);