<?php
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require_once "./baseCallHandler.php";
require_once "../controllers/aboutController.php";

handleCall(
    "GET",
    [profileCodes::ANONYMOUS],
    new AboutController(),
    "getAbout"
);
