<?php
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require_once "./baseCallHandler.php";
require_once "../controllers/accountController.php";

handleCall(
    "POST",
    [profileCodes::ANONYMOUS],
    new AccountController(),
    "login"
);