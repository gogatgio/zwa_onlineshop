<?php
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require_once "./baseCallHandler.php";
require_once "../controllers/productController.php";

handleCall(
    "POST",
    [profileCodes::BASIC, profileCodes::MANAGER],
    new ProductController(),
    "orderProduct",
    false
);
