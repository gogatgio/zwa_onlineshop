<?php
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require_once "./baseCallHandler.php";
require_once "../controllers/logController.php";

handleCall(
    "POST",
    [profileCodes::ANONYMOUS],
    new LogController(),
    "log",
    false
);
