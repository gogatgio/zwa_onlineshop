<?php
/**
 * Log origins: server / client
 */
abstract class logOrigins {
    const server = "server";
    const client = "client";
}