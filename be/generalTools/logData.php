<?php
/**
 * Contains data that will be logged to DB.
 */
class logData {
    public $utcdatetime;
    public $origin;
    public $level;
    public $message;
    public $stack;

    function __construct($utcdatetime, $origin, $level, $message, $stack) {
        $this->utcdatetime = $utcdatetime;
        $this->origin = $origin;
        $this->level = $level;
        $this->message = $message;
        $this->stack = $stack;
    }

    /**
     * Creates and returns a logData instance from anonymous object
     * that was created from parsing the request json.
     */
    static function getLogData($decodedRequestLogData) {
        return new self(
            isset($decodedRequestLogData->utcdatetime) ? $decodedRequestLogData->utcdatetime : null,
            isset($decodedRequestLogData->origin) ? $decodedRequestLogData->origin : null,
            isset($decodedRequestLogData->level) ? $decodedRequestLogData->level : null,
            isset($decodedRequestLogData->message) ? $decodedRequestLogData->message : null,
            isset($decodedRequestLogData->stack) ? $decodedRequestLogData->stack : null
        );
    }
}