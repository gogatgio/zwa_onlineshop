<?php
/**
 * Status codes for http response.
 */
abstract class responseStatusCodes {
    const ok = 200;

    const badRequest = 400;
    const unauthorized = 401;
    const forbidden = 403;
    const notFound = 404;
    const methodNotAllowed = 405;

    const internalServerError = 500;
}