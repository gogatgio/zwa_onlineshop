<?php

/**
 * Class providing methods to help working with jwt tokens (bearer tokens).
 */
class JwtHelper {
    /**
     * Encodes the string to base64 and replaces url special chars.
     * @return string
     */
    function base64UrlEncode($string) {
        return str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($string));
    }

    /**
     * Replaces url special chars and decodes base64 string.
     * @return string
     */
    function base64UrlDecode($string) {
        return base64_decode(str_replace(['-', '_'], ['+', '/'], $string));
    }

    /** 
     * Get authorization header
     * @return string or null if nothing found 
     */
    function getAuthorizationHeader() {
        $authorizationHeader = null;

        if (isset($_SERVER['Authorization'])) {
            $authorizationHeader = trim($_SERVER["Authorization"]);
        } else if (isset($_SERVER['HTTP_AUTHORIZATION'])) {
            $authorizationHeader = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            if (isset($requestHeaders['Authorization'])) {
                $authorizationHeader = trim($requestHeaders['Authorization']);
            }
        }

        return $authorizationHeader;
    }

    /**
     * Gets bearer token from authorization header
     * @return string or null if nothing found
     */
    function getBearerToken() {
        $authorizationHeader = $this->getAuthorizationHeader();
        if (!empty($authorizationHeader)) {
            if (preg_match('/Bearer\s(\S+)/', $authorizationHeader, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }

    /**
     * Generates random string
     * @return string
     */
    function generateJwtSecret() {
        return base64_encode(random_bytes(12));
    }

    /**
     * Generates JWT
     * @return string
     */
    function generateZwaJwt(ZwaJwtPayload $payload, $secret) {
        $header = ['typ' => 'JWT', 'alg' => 'HS256'];
        $jwt = $this->generateJwt($header, $payload, $secret);
        return $jwt;
    }

    /**
     * Generates JWT
     * @return string
     */
    function generateJwt($header, $payload, $secret) {
        $encodedHeader = $this->base64UrlEncode(json_encode($header));
        $encodedPayload = $this->base64UrlEncode(json_encode($payload));
        $encodedSignature = $this->createSignature($encodedHeader, $encodedPayload, $secret);

        $jwt = "$encodedHeader.$encodedPayload.$encodedSignature";

        return $jwt;
    }

    /**
     * Creates a signature using the encodedHeader and the encodedPayload and the secret.
     * @return string
     */
    function createSignature($encodedHeader, $encodedPayload, $secret) {
        $signature = hash_hmac('SHA256', "$encodedHeader.$encodedPayload", $secret, true);
        $encodedSignature = $this->base64UrlEncode($signature);
        return $encodedSignature;
    }

    /**
     * Checks if expiration is set and it's not expired.
     * @return boolean true if valid. false if expired
     */
    function validateJwtExpiration($jwt) {
        // Split JWT and decode the payload
        $jwtParts = explode('.', $jwt);
        $jwtPayload = base64_decode($jwtParts[1]);
        $decodedJwtPayload = json_decode($jwtPayload);

        // Check if expiration set
        if (!isset($decodedJwtPayload->expiration)) {
            return false;
        }

        // Check expiration date
        $expiration = $decodedJwtPayload->expiration;
        if (new DateTime($expiration->date) < new DateTime()) {
            return false;
        }

        // Everything OK
        return true;
    }

    /**
     * Recretes and compares the jwt signature.
     * @return boolean true if valid. false if invalid signature
     */
    function validateJwtSignature($jwt, $secret) {
        // Split JWT
        $jwtParts = explode('.', $jwt);
        $jwtHeader = base64_decode($jwtParts[0]);
        $jwtPayload = base64_decode($jwtParts[1]);
        $jwtSignature = $jwtParts[2];

        // Recreate signature
        $encodedHeader = $this->base64UrlEncode($jwtHeader);
        $encodedPayload = $this->base64UrlEncode($jwtPayload);
        $recreatedEncodedSignature = $this->createSignature($encodedHeader, $encodedPayload, $secret);

        // Compare signatures
        if ($recreatedEncodedSignature !== $jwtSignature) {
            return false;
        }

        // Everything OK
        return true;
    }

    /**
     * @return decoded jwt payload as an anonymous object
     */
    function getJwtPayload($jwt) {
        $encodedPayload = explode('.', $jwt)[1];
        $payloadJson = $this->base64UrlDecode($encodedPayload);
        $payload = json_decode($payloadJson);
        return $payload;
    }
}

/**
 * Class containing jwt data.
 */
class ZwaJwtPayload {
    public $accountId;
    public $email;
    public $profiles;
    public $sessionId;
    public $expiration;

    function __construct($accountId, $email, $profiles, $sessionId, $expiration) {
        $this->accountId = $accountId;
        $this->email = $email;
        $this->profiles = $profiles;
        $this->sessionId = $sessionId;
        $this->expiration = $expiration;
    }
}
