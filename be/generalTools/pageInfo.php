<?php
/**
 * Contains page info data - pageIndex, pageSize, pageCount
 */
class pageInfo {
    public $pageIndex;
    public $pageSize;
    public $pageCount;

    function __construct($pageIndex, $pageSize, $pageCount) {
        $this->pageIndex = $pageIndex;
        $this->pageSize = $pageSize;
        $this->pageCount = $pageCount;
    }
}