<?php
/**
 * Log levels corresponding to FE logLovels
 */
abstract class logLevels {
    const error = 4;
    const warning = 3;
    const information = 2;
    const debug = 1;
    const trace = 0;

    static public function getLogLevelName($logLevel) {
        switch ($logLevel) {
            case logLevels::error:
                return "ERROR";
            case logLevels::warning:
                return "WARNING";
            case logLevels::information:
                return "INFORMATION";
            case logLevels::debug:
                return "DEBUG";
            case logLevels::trace:
                return "TRACE";
        }
    }
}