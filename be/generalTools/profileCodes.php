<?php
/**
 * Profile codes
 */
abstract class profileCodes {
    const ANONYMOUS = "ANONYMOUS";
    const BASIC = "BASIC";
    const MANAGER = "MANAGER";
}