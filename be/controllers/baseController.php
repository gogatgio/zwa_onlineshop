<?php
abstract class baseController {
    /**
     * Checks wheter a param exists in the array. If not deafult is returned instead.
     * Empty strings aren't considered valid, so deafult is returend as well.
     * @param default if not stated null is used
     */
    protected function getParamOrDeafult($paramKey, $array, $default = null) {
        if (array_key_exists($paramKey, $array) && !empty($array[$paramKey])) {
            return $array[$paramKey];
        } else {
            return $default;
        }
    }

    /**
     * Returns an object containing the decoded data from request body json.
     */
    protected function getRequestBodyJsonData() {
        $json = file_get_contents('php://input');
        return json_decode($json);
    }

    /**
     * Formats DateTime to string using "d/m/Y H:i:s".
     */
    protected function formatDateTime(DateTime $dateTime) {
        return $dateTime->format("d/m/Y H:i:s");
    }
}
