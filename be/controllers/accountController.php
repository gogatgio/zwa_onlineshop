<?php
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require_once "baseController.php";
require_once "../generalTools/responseStatusCodes.php";
require_once "dtos/loginDtoIn.php";
require_once "dtos/registerDtoIn.php";
require_once "../validations/validator.php";
require_once "../validations/accountValidationTypes.php";
require_once "../repositories/accountRepository.php";
require_once "../generalTools/jwtHelper.php";
require_once "dtos/loginDtoOut.php";
require_once "dtos/registerDtoOut.php";

class AccountController extends  baseController{    
    /**
     * This funtions gets called through http request.
     */
    function login() {
        // Get login data
        $loginData = loginDtoIn::getDtoIn($this->getRequestBodyJsonData());

        // Validate loginData
        $validator = new Validator();
        $validator->validate($loginData, new loginDtoInType());

        $accountRepository = new AccountRepository();

        // Get account
        $account = $accountRepository->getAccount($loginData->loginEmail);
        if (!$account) {
            throw new zwaGeneralException(responseStatusCodes::notFound, "Account not found.");
        }
        $accountId = (int)$account->id;

        // Verify password
        $passwordCorrect = password_verify($loginData->loginPassword, $account->passwordHash);
        if (!$passwordCorrect) {
            $validationResult = new stdClass();
            $validationResult->key = "loginPassword";
            $validationResult->errors = array("Chybné heslo nebo email.");
            $validationResults = array($validationResult);
            $validator->throwZwaValidationException($validationResults);
        }

        // Get account profiles
        $accountProfileList = $accountRepository->getAccountProfileList($accountId);

        // Create new login session and generate new jwt
        $jwt = $this->createLoginSession($accountRepository, $accountId, $account->email, $accountProfileList);

        return new LoginDtoOut($jwt);
    }

    /**
     * This funtions gets called through http request.
     */
    function register() {
        // Get register data
        $registerData = registerDtoIn::getDtoIn($this->getRequestBodyJsonData());

        // Validate registerData
        $validator = new Validator();
        $validator->validate($registerData, new registerDtoInType());

        $accountRepository = new AccountRepository();

        // Check email duplicity
        $duplicitEmail = $accountRepository->checkEmailDuplicity($registerData->registerEmail);
        if ($duplicitEmail) {
            $validationResult = new stdClass();
            $validationResult->key = "registerEmail";
            $validationResult->errors = array("Email je obsazen.");
            $validationResults = array($validationResult);
            $validator->throwZwaValidationException($validationResults);
        }

        // Create new account
        $passwordHash = password_hash($registerData->registerPassword1, PASSWORD_DEFAULT);
        $accountId = $accountRepository->createAccount($registerData->registerEmail, $passwordHash);

        // Get basic profile
        $basicProfile = $accountRepository->getProfile(profileCodes::BASIC);
        if (!$basicProfile) {
            throw new zwaGeneralException(responseStatusCodes::notFound, "Basic profile not found.");
        }
        $profileId = (int)$basicProfile->id;

        // Assign basic account profile
        $accountProfileId = $accountRepository->createAccountProfile($accountId, $profileId);

        // Create new login session and generate new jwt
        $jwt = $this->createLoginSession($accountRepository, $accountId, $registerData->registerEmail, [profileCodes::BASIC]);

        return new RegisterDtoOut($jwt);
    }

    /**
     * Creates new login session and generate new jwt
     * @return string jwt
     */
    function createLoginSession($accountRepository, $accountId, $accountEmail, $accountProfileList) {
        $jwtHelper = new JwtHelper();

        $utcExpirationDatetime = (new DateTime())->add(new DateInterval("P1M"));
        $jwtSecret = $jwtHelper->generateJwtSecret();
        $loginSessionId = $accountRepository->createLoginSession($accountId, $utcExpirationDatetime, $jwtSecret);
        $jwtPayload = new ZwaJwtPayload(
            $accountId,
            $accountEmail,
            $accountProfileList,
            $loginSessionId,
            $utcExpirationDatetime
        );
        $jwt = $jwtHelper->generateZwaJwt($jwtPayload, $jwtSecret);

        return $jwt;
    }

    /**
     * @return string or null if nothing found
     */
    function getLoginSessionJwtSecret($sessionId) {
        $accountRepository = new AccountRepository();
        $loginSession = $accountRepository->getLoginSession($sessionId);
        if (!$loginSession) {
            return null;
        }
        return $loginSession->jwtSecret;
    }

    /**
     * Deletes the login session.
     * This funtions gets called through http request.
     */
    function logout() {
        global $authentication;

        $accountRepository = new AccountRepository();

        $accountRepository->deleteLoginSession($authentication->sessionId);
    }

    /**
     * This is just to check token validity.
     * @return nothing
     */
    function checkLogin() {

    }
}
