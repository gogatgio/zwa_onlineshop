<?php
/**
 * Contains register data.
 */
class registerDtoIn {
    public $registerEmail;
    public $registerPassword1;
    public $registerPassword2;

    function __construct($registerEmail, $registerPassword1, $registerPassword2) {
        $this->registerEmail = $registerEmail;
        $this->registerPassword1 = $registerPassword1;
        $this->registerPassword2 = $registerPassword2;
    }

    /**
     * Creates and returns a registerDtoIn instance from anonymous object
     * that was created from parsing the request json.
     */
    static function getDtoIn($decodedRequestRegisterData) {
        return new self(
            isset($decodedRequestRegisterData->registerEmail) ? $decodedRequestRegisterData->registerEmail : null,
            isset($decodedRequestRegisterData->registerPassword1) ? $decodedRequestRegisterData->registerPassword1 : null,
            isset($decodedRequestRegisterData->registerPassword2) ? $decodedRequestRegisterData->registerPassword2 : null
        );
    }
}