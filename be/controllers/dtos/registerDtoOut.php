<?php
/**
 * Contains bearerToken.
 */
class RegisterDtoOut {
    public $bearerToken;

    function __construct($bearerToken) {
        $this->bearerToken = $bearerToken;
    }
}