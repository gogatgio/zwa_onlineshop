<?php
/**
 * Contains getProductList data.
 */
class getProductListDtoIn {
    public $sort;
    public $search;
    public $pageIndex;
    public $pageSize;

    function __construct($sort, $search, $pageIndex, $pageSize) {
        $this->sort = $sort;
        $this->search = $search;
        $this->pageIndex = $pageIndex;
        $this->pageSize = $pageSize;
    }
}