<?php
/**
 * Contains addProduct data.
 */
class addProductDtoIn {
    public $addProductName;
    public $addProductDescription;
    public $addProductPrice;
    public $addProductStock;

    function __construct($addProductName, $addProductDescription, $addProductPrice, $addProductStock) {
        $this->addProductName = $addProductName;
        $this->addProductDescription = $addProductDescription;
        $this->addProductPrice = $addProductPrice;
        $this->addProductStock = $addProductStock;
    }

    /**
     * Creates and returns a addProductDtoIn instance from anonymous object
     * that was created from parsing the request json.
     */
    static function getDtoIn($decodedRequestAddProductData) {
        return new self(
            isset($decodedRequestAddProductData->addProductName) ? $decodedRequestAddProductData->addProductName : null,
            isset($decodedRequestAddProductData->addProductDescription) ? $decodedRequestAddProductData->addProductDescription : null,
            isset($decodedRequestAddProductData->addProductPrice) ? $decodedRequestAddProductData->addProductPrice : null,
            isset($decodedRequestAddProductData->addProductStock) ? $decodedRequestAddProductData->addProductStock : null
        );
    }
}