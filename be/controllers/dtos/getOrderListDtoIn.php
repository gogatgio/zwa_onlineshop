<?php
/**
 * Contains getOrderList data.
 */
class GetOrderListDtoIn {
    public $sort;
    public $pageIndex;
    public $pageSize;

    function __construct($sort, $pageIndex, $pageSize) {
        $this->sort = $sort;
        $this->pageIndex = $pageIndex;
        $this->pageSize = $pageSize;
    }
}