<?php
/**
 * Contains product list.
 */
class GetOrderListDtoOut {
    public $orderList;
    public $pageInfo;
    
    function __construct($orderList, $pageInfo) {
        $this->orderList = array_map(function($order) {
            $order->orderPrice = round($order->orderPrice, 2);
            return $order;
        }, $orderList);
        $this->pageInfo = $pageInfo;
    }
}