<?php
/**
 * Contains bearerToken.
 */
class LoginDtoOut {
    public $bearerToken;

    function __construct($bearerToken) {
        $this->bearerToken = $bearerToken;
    }
}