<?php
/**
 * Contains orderProduct data.
 */
class orderProductDtoIn {
    public $orderProductId;
    public $orderProductCount;
    public $orderProductCurrency;

    function __construct($orderProductId, $orderProductCount, $orderProductCurrency) {
        $this->orderProductId = $orderProductId;
        $this->orderProductCount = $orderProductCount;
        $this->orderProductCurrency = $orderProductCurrency;
    }

    /**
     * Creates and returns a orderProductDtoIn instance from anonymous object
     * that was created from parsing the request json.
     */
    static function getDtoIn($decodedRequestOrderProductData) {
        return new self(
            isset($decodedRequestOrderProductData->orderProductId) ? $decodedRequestOrderProductData->orderProductId : null,
            isset($decodedRequestOrderProductData->orderProductCount) ? $decodedRequestOrderProductData->orderProductCount : null,
            isset($decodedRequestOrderProductData->orderProductCurrency) ? $decodedRequestOrderProductData->orderProductCurrency : null
        );
    }
}