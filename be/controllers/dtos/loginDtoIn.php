<?php
/**
 * Contains login data.
 */
class loginDtoIn {
    public $loginEmail;
    public $loginPassword;

    function __construct($loginEmail, $loginPassword) {
        $this->loginEmail = $loginEmail;
        $this->loginPassword = $loginPassword;
    }

    /**
     * Creates and returns a loginDtoIn instance from anonymous object
     * that was created from parsing the request json.
     */
    static function getDtoIn($decodedRequestLoginData) {
        return new self(
            isset($decodedRequestLoginData->loginEmail) ? $decodedRequestLoginData->loginEmail : null,
            isset($decodedRequestLoginData->loginPassword) ? $decodedRequestLoginData->loginPassword : null
        );
    }
}