<?php
/**
 * Contains product list.
 */
class GetProductListDtoOut {
    public $productList;
    public $pageInfo;
    
    function __construct($productList, $pageInfo) {
        $this->productList = $productList;
        $this->pageInfo = $pageInfo;
    }
}