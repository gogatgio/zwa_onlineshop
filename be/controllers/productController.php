<?php
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require_once "baseController.php";
require_once "../validations/validator.php";
require_once "../validations/productValidationTypes.php";
require_once "../repositories/productRepository.php";
require_once "dtos/getProductListDtoIn.php";
require_once "dtos/getProductListDtoOut.php";
require_once "dtos/addProductDtoIn.php";
require_once "dtos/getProductDtoIn.php";
require_once "dtos/orderProductDtoIn.php";
require_once "../repositories/orderRepository.php";
require_once "../generalTools/currencyCodes.php";
require_once "../repositories/currencyRepository.php";

define("deafultPageIndex", 0);
define("deafultPageSize", 12);

// TODO get this value from internet daily
define("czkToEurExchangeRate", 0.043);

class ProductController extends  baseController{
    function getProductList() {
        // Get params
        $sort = $this->getParamOrDeafult("sort", $_GET);
        $search = $this->getParamOrDeafult("search", $_GET);
        $pageIndex = (int)$this->getParamOrDeafult("pageIndex", $_GET, deafultPageIndex);
        $pageSize = (int)$this->getParamOrDeafult("pageSize", $_GET, deafultPageSize);
        
        // Validate params
        $getProductListDtoIn = new getProductListDtoIn($sort, $search, $pageIndex, $pageSize);
        $validator = new Validator();
        $validator->validate($getProductListDtoIn, new getProductListDtoInType());

        // Get product list
        $productRepository = new ProductRepository();
        $productList = $productRepository->getProductList($getProductListDtoIn);
        $pageInfo = $productRepository->getPageInfo($getProductListDtoIn);
        $GetProductListDtoOut = new GetProductListDtoOut($productList, $pageInfo);

        // Return result
        return $GetProductListDtoOut;
    }

    function getProduct() {
        // Get id
        $id = (int)$this->getParamOrDeafult("id", $_GET, 0);
        
        // Validate params
        $getProductDtoIn = new getProductDtoIn($id);
        $validator = new Validator();
        $validator->validate($getProductDtoIn, new getProductDtoInType());

        // Get product
        $productRepository = new ProductRepository();
        $product = $productRepository->getProduct($getProductDtoIn->id);
        if(!$product) {
            throw new zwaGeneralException(responseStatusCodes::notFound, "Product not found.");
        }
        $product->priceEur =  round($product->price * czkToEurExchangeRate, 2);

        // Return result
        return $product;
    }

    function orderProduct() {
        // Get orderProduct data
        $orderProductData = orderProductDtoIn::getDtoIn($this->getRequestBodyJsonData());

        // Validate orderProductData
        $validator = new Validator();
        $validator->validate($orderProductData, new orderProductDtoInType());

        $productRepository = new ProductRepository();

        // TODO implement locking (transactions) to prevent concurrency errors
        
        // Get product
        $productRepository = new ProductRepository();
        $product = $productRepository->getProduct($orderProductData->orderProductId);
        if (!$product) {
            throw new zwaGeneralException(responseStatusCodes::notFound, "Product not found.");
        }

        // Check stock
        if ($orderProductData->orderProductCount > $product->stock) {
            throw new zwaGeneralException(responseStatusCodes::badRequest, "You can't order more that there is in the stock.");
        }

        // Create Order
        global $authentication;

        $priceMultiplier = $orderProductData->orderProductCurrency == currencyCodes::EUR ? czkToEurExchangeRate : 1;
        $orderPrice = round($orderProductData->orderProductCount * (int)$product->price * $priceMultiplier, 2);

        $currencyRepository = new CurrencyRepository();
        $currency = $currencyRepository->getCurrency($orderProductData->orderProductCurrency);
        if (!$currency) {
            throw new zwaGeneralException(responseStatusCodes::notFound, "Currency not found.");
        }
        $currencyId = (int)$currency->id;

        $orderRepository = new OrderRepository();
        $orderId = $orderRepository->createOrder($authentication->accountId, $product->id, $orderProductData->orderProductCount, $orderPrice, $currencyId);

        // Reduce stock
        $productRepository->updateProductStock($orderProductData->orderProductId, -$orderProductData->orderProductCount);
    }

    function addProduct() {
        // Get addProduct data
        $addProductData = addProductDtoIn::getDtoIn($this->getRequestBodyJsonData());

        // Validate addProductData
        $validator = new Validator();
        $validator->validate($addProductData, new addProductDtoInType());

        // Add new product
        $productRepository = new ProductRepository();
        $productId = $productRepository->addProduct($addProductData);
    }
}
