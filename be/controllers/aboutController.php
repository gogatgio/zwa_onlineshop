<?php
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require_once "baseController.php";
require_once "../config.php";

class AboutController extends  baseController{
    function getAbout() {
        // Get git details
        $gitUrlCommand = 'git config --get remote.origin.url';
        $gitUrl = exec(ENVIRONMENT === 'local' ?
            $gitUrlCommand :
            'echo "'.TOAD_PASSWORD.'" | su -c "'.$gitUrlCommand.'" '.TOAD_USER);
        
        $gitBranchCommand = 'git rev-parse --abbrev-ref HEAD';
        $gitBranch = exec(ENVIRONMENT === 'local' ?
            $gitBranchCommand :
            'echo "'.TOAD_PASSWORD.'" | su -c "'.$gitBranchCommand.'" '.TOAD_USER);

        $commitDateCommand = 'git log --pretty="%ci" -n1 HEAD';
        $commitDate = exec(ENVIRONMENT === 'local' ?
            $commitDateCommand :
            'echo "'.TOAD_PASSWORD.'" | su -c "'.$commitDateCommand.'" '.TOAD_USER);

        $commitHashCommand = 'git log --pretty="%H" -n1 HEAD';
        $commitHash = exec(ENVIRONMENT === 'local' ?
            $commitHashCommand :
            'echo "'.TOAD_PASSWORD.'" | su -c "'.$commitHashCommand.'" '.TOAD_USER);

        $formattedCommitDate = $this->formatDateTime(new \DateTime($commitDate));

        $getAboutResponse = new getAboutResponse($gitUrl, $gitBranch, $formattedCommitDate, $commitHash);

        // Return result
        return $getAboutResponse;
    }
}

class getAboutResponse {
    public $gitUrl;
    public $gitBranch;
    public $commitDate;
    public $commitHash;
    function __construct($gitUrl, $gitBranch, $commitDate, $commitHash) {
        $this->gitUrl = $gitUrl;
        $this->gitBranch = $gitBranch;
        $this->commitDate = $commitDate;
        $this->commitHash = $commitHash;
    }
}
