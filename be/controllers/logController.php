<?php
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require_once "baseController.php";
require_once "../validations/validator.php";
require_once "../validations/logValidationTypes.php";
require_once "../repositories/logRepository.php";
require_once "../generalTools/logData.php";

class LogController extends  baseController{
    /**
     * This funtions gets called through http request.
     */
    function log() {
        // Get log data
        $logData = logData::getLogData($this->getRequestBodyJsonData());

        // Validate logData
        $validator = new Validator();
        $validator->validate($logData, new logDtoInType());

        $this->beLog($logData);
    }

    /**
     * This function gets called from BE.
     */
    function beLog(logData $logData) {
        $logRepository = new LogRepository();
        $logRepository->log($logData);
    }
}
