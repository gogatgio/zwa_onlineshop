<?php
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require_once "baseController.php";
require_once "../validations/validator.php";
require_once "../validations/orderValidationTypes.php";
require_once "../repositories/orderRepository.php";
require_once "dtos/getOrderListDtoIn.php";
require_once "dtos/getOrderListDtoOut.php";
require_once "dtos/deleteOrderDtoIn.php";
require_once "../repositories/productRepository.php";

define("deafultPageIndex", 0);
define("deafultPageSize", 12);

class OrderController extends  baseController{
    function getOrderList() {
        // Get params
        $sort = $this->getParamOrDeafult("sort", $_GET);
        $pageIndex = (int)$this->getParamOrDeafult("pageIndex", $_GET, deafultPageIndex);
        $pageSize = (int)$this->getParamOrDeafult("pageSize", $_GET, deafultPageSize);
        
        // Validate params
        $getOrderListDtoIn = new GetOrderListDtoIn($sort, $pageIndex, $pageSize);
        $validator = new Validator();
        $validator->validate($getOrderListDtoIn, new GetOrderListDtoInType());

        // Get order list
        global $authentication;
        $orderRepository = new OrderRepository();
        $orderList = $orderRepository->getOrderList($authentication->accountId, $getOrderListDtoIn);
        $pageInfo = $orderRepository->getPageInfo($authentication->accountId, $getOrderListDtoIn);
        $getOrderListDtoOut = new GetOrderListDtoOut($orderList, $pageInfo);

        // Return result
        return $getOrderListDtoOut;
    }

    function deleteOrder() {
        // Get id
        $id = (int)$this->getParamOrDeafult("id", $_GET, 0);

        // Validate params
        $deleteOrderDtoIn = new DeleteOrderDtoIn($id);
        $validator = new Validator();
        $validator->validate($deleteOrderDtoIn, new DeleteOrderDtoInType());

        global $authentication;
        $orderRepository = new OrderRepository();

        // Check order exists
        $order = $orderRepository->getOrder($authentication->accountId, $deleteOrderDtoIn->id);
        if(!$order) {
            throw new zwaGeneralException(responseStatusCodes::notFound, "Order not found.");
        }

        // Delete order
        $rowsAffected = $orderRepository->deleteOrder($authentication->accountId, $deleteOrderDtoIn->id);

        // Increase stock
        $productRepository = new ProductRepository();
        $productRepository->updateProductStock($order->productId, $order->productCount);
    }
}
